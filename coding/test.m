r = 1000;
c = 1000;
matrix = bsxfun(@plus, 0:c-1, (1:r)');

for i = 1:r
  for j = 1:c
    printf("%d ", matrix(i, j))
  endfor
  printf("\n")
endfor