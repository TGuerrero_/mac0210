#{
Exemplo 1.2: cálculo de f'(x0)
para f=sen e x0=1.2
usando a aproximação de Taylor
f'(x0) ~ (sin(x0+h)-sin(x0))/h
#}

x0 = 1.2

# f'(x0)=cos(x0) é a "resposta certa" (ground truth) que gostaríamos de obter com a aproximação de Taylor
valor_exato = cos(x0)

# tentativas isoladas (compare com o valor do ground truth)
h = 0.1;
valor_aprox = (sin(x0+h)-sin(x0))/h
h = 0.01;
valor_aprox = (sin(x0+h)-sin(x0))/h
h = 0.001;
valor_aprox = (sin(x0+h)-sin(x0))/h

# calculando para h = 1e-1,1e-2,...,1e-7
h = 10.^(-1:-1:-7)
valor_aprox = (sin(x0+h)-sin(x0))./h

#{
Uma pergunta interessante é: qual seria o valor
de h necessário pra produzir uma aproximação
com erro menor do que 10^(-10)?
#}
# medida de erro absoluto:
erro_abs = abs(valor_exato-valor_aprox)

#{
Pela teoria, podemos estimar o erro pelo termo
dominante, que é -h*f''(x0)/2. Nesse exemplo, por
sorte, podemos calcular f''(x0) = -sin(x0).
#}
erro_teorico_aprox = abs(-h*(-sin(x0))/2)

# comparação entre os dois erros com plot simples (eixos lineares):
plot(h,erro_abs,h,erro_teorico_aprox)

#{
Os eixos lineares fazem com que a maioria
das observações (h=1e-3, 1e-4,...) fique
espremida no canto esquerdo do gráfico.
Uma forma de visualização melhor é trocar
os dois eixos para log:
#}
loglog(h,erro_abs,h,erro_teorico_aprox)
# anotações no gráfico:
xlabel('h')
ylabel('erro absoluto e erro teórico')
title("aproximação de f'(x0) usando Taylor")

#{
Retomando a pergunta: qual seria o valor de h
necessário pra produzir uma aproximação com
erro menor do que 10^(-10)?
Poderíamos usar a expressão do termo dominante
do erro e encontrar um valor que torne aquele
termo menor do que 1e-10:
-h*(-sin(x0))/2 < 1e-10 <==>
<==> h < 1e-10*2/sin(x0)
Por exemplo:
#}
h = 1e-10/sin(x0)
valor_aprox = (sin(x0+h)-sin(x0))./h
erro_abs = abs(cos(x0)-(sin(x0+h)-sin(x0))./h)
# ainda não funciona...

h = 1e-11/sin(x0)
valor_aprox = (sin(x0+h)-sin(x0))./h
erro_abs = abs(cos(x0)-(sin(x0+h)-sin(x0))./h)
# piorou...

# para ganhar uma visão mais geral:
h = 10.^(-1:-1:-20)
valor_aprox = (sin(x0+h)-sin(x0))./h
erro_abs = abs(valor_exato-valor_aprox)
erro_teorico_aprox = abs(-h*(-sin(x0))/2)
loglog(h,erro_abs,h,erro_teorico_aprox)
xlabel('h')
ylabel('erro absoluto e erro teórico')
title("aproximação de f0'(x0) usando Taylor")
#{
Fizemos vários comentários em aula sobre
a figura anterior, mas o resumo é que os erros
se comportam bastante de acordo com a teoria
para h=1e-1,1e-2,...,1e-8, mas para valores
menores de h os erros de arredondamento,
associados às operações aritméticas em precisão
finita, começam a dominar o resultado, até que
ele passa a ser inútil.
#}
# Exemplos de flags de formatação da figura
loglog(h,erro_abs,'-*',h,erro_teorico_aprox,'r-.')
# use esse comando para navegar pela documentação sobre gráficos:
doc plot

