2;
#{
  yn = integral(x^n/(x+10))
#}

function result = y(n)
  if n == 0
    result = log(11) - log(10);
  else
    result = 1/(n) - 10*y(n-1);
  endif
endfunction

function result = yreverse(n)
  if n == 31
    result = 0.002848777467515764;
  else
    result = (1/10)*(1/(n+1) - yreverse(n+1));
  endif
endfunction

results = [];
results_reverse = [];
for n = 0:30
  results = [results y(n)];
  results_reverse = [results_reverse yreverse(n)];
endfor

results
results_reverse

n = 0:30;
figure(1)
plot(n, results, "r", n, results_reverse, "g")
title("All")
figure(2)
plot(n(1:16), results(1:16), "r", n(1:16), results_reverse(1:16), "g")
title("0:15")
