function [x, n] = newton(f, df, x0, atol)
  n = 0;
  do
    if (df(x0) < eps)
      x = x0;
      display('Derivada nula');
      return;
    endif
    x = x0 - (f(x0)/df(x0));
    diff = abs(x - x0);
    x0 = x;
    n++;
  until diff < atol
endfunction
