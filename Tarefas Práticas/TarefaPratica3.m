#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Nome : Thiago Guerrero
NUSP : 11275297
Matéria: MAC0210
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 3 é explorar a aplicação do Método de Newton ao problema
de minimizar uma função f(x) genérica. Como visto em aula, todo mínimo local x* de f(·)
é um ponto crítico, ou seja, satisfaz f'(x*)=0. Além disso, uma condição suficiente para
que um ponto crítico seja mínimo é satisfazer f"(x*)>0. A tarefa está organizada em 4 itens,
descritos abaixo.
#}

0; # força o Octave a interpretar esse arquivo como um script file (ver https://octave.org/doc/v5.2.0/Script-Files.html)

#{

Item 1: Implementação do método (feito em aula)

Escreva uma função Minimiza que recebe uma função f() e duas derivadas df()=f'() e ddf()=f"(),
um ponto inicial x0 e uma tolerância atol, e aplica o método de Newton para a equação f'(x)=0:

x ← x-f'(x)/f"(x)

interrompendo a sequência quando a diferença absoluta entre iterados sucessivos for menor do que
atol (critério de convergência), ou quando a próxima iteração do método exigisse uma divisão por zero.

Sua função deve devolver:
x = último iterado
n = número de iterações realizadas
v = valor de função obtido
t = tipo de terminação (t=1 indica que x é ponto de mínimo, t=-1 indica máximo, t=0 indica outro
tipo de ponto crítico, e t=NaN indica que o método foi interrompido antes do critério de convergência).

#}
function [x,n,v,t] = Minimiza(f,df,ddf,x0,atol)
  n = 0;
  do
    if (abs(ddf(x0)) < eps)
      x = x0;
      v = f(x);
      t = NaN;
      return;
    endif
    x = x0 - df(x0)/ddf(x0);
    diff = abs(x - x0);
    x0 = x;
    n++;
  until (diff < atol)
  v = f(x);
  t = sign(ddf(x));
endfunction

#{

Item 2: Teste do método

Execute sua função nos casos de teste abaixo, e comente no código os resultados. Em particular, 
explique sucintamente o que ocorreu nos casos em que o método NÃO encontrou um ponto de mínimo.

#}

## exemplo de funções de teste
function y=f(x)   y=sin(x);  end
function y=df(x)  y=cos(x);  end
function y=ddf(x) y=-sin(x); end

## códigos de teste
[x,n,v,t] = Minimiza(@f,@df,@ddf,0,1e-8) # NaN
[x,n,v,t] = Minimiza(@f,@df,@ddf,0.1,1e-8) # 10.96, t = 1, n = 6
[x,n,v,t] = Minimiza(@f,@df,@ddf,1,1e-8) # 1.57, t = -1, n = 4
[x,n,v,t] = Minimiza(@f,@df,@ddf,5,1e-8) # 4.71, t = 1, n = 4

#{
Comentários:
  
  No primeiro caso não conseguimos uma resposta válida pois caímos no caso em que f''(x0) = 0
já no ponto inicial x0 = 0. Portanto, o método de Newton é incapaz de achar um ponto crítico pois
não é possível fazer a divisão f'(x0)/f''(x0) fazendo com que o algoritmo, por uma questão de robustez,
pare.

  Plotando os gráficos de f, f' e f'' é possível entender um pouco o comportamento do método.
Quando escolhemos valores de x0 próximos de algum ponto de máximo ou mínimo de f, temos que o gráfico de
f' estara convergindo rapidamente para 0, como mostra o valor de f'' nesses pontos (que estará assumindo
seu valor máximo ou mínimo). Logo, a reta tangente a esse ponto terá uma inclinação alta fazendo com que
o método convirga para o ponto crítico mais próximo, como ocorre nos exemplos com x0 = 1 e x0 = 5. Entretanto,
como o método de Newton tem como fundamento somente achar raízes, ou seja, pontos críticos, não temos como
garantir que esse ponto será de mínimo, como ocorre no caso x0 = 1, que econtramos um ponto crítico, mas o mesmo
é ponto de máximo.

  Já nos pontos mais distantes de um ponto de máximo ou mínimo de f, temos que o gráfico de f' não estará
convergindo tão rapidamente para 0 pois o valor de f'' nesse ponto estará tendendo a 0. Logo, a reta tangente
nesse ponto não possui uma inclinação muito alta, fazendo com que a raiz da reta tangente seja distante do ponto
inicial e fazendo o método convergir para um ponto que não está necessariamente próximo do ponto inicial,
como ocorre no x0 = 0.1.
#}
  
#{

Item 3: Melhorando a convergência do método

Uma forma de evitar as situações encontradas no item 2 consiste em interceptar as iterações
do método de Newton em que f"(x)<0, que poderiam levar a sequência a um ponto pior, e mudar
a definição do método substituindo f"(x) por 1 no denominador:

x ← x-f'(x)

Essa iteração está associada ao Método de Cauchy ou Método da Descida do Gradiente. A mesma iteração
poderia ser usada quando f"(x)≈0, um caso em que o método de Newton não está definido.

Re-escreva o método do Item 1 com essas duas modificações e execute-o nos exemplos de teste do Item 2,
comentando os resultados obtidos.

#}

function [x,n,v,t] = Minimiza2(f,df,ddf,x0,atol)
  n = 0;
  do
    if (abs(ddf(x0)) < eps || ddf(x0) < 0)
      x = x0 - df(x0);
    else
      x = x0 - df(x0)/ddf(x0);
    endif
    diff = abs(x - x0);
    x0 = x;
    n++;
  until (diff < atol)
  v = f(x);
  t = sign(ddf(x));
endfunction

## códigos de teste
[x,n,v,t] = Minimiza2(@f,@df,@ddf,0,1e-8) # -1.57, t = 1, n = 5
[x,n,v,t] = Minimiza2(@f,@df,@ddf,0.1,1e-8) # -1.57, t = 1, n = 5
[x,n,v,t] = Minimiza2(@f,@df,@ddf,1,1e-8) # -1.57, t = 1, n = 8
[x,n,v,t] = Minimiza2(@f,@df,@ddf,5,1e-8) # 4.71, t = 1, n = 4

#{
Comentários:

  É trivial ver que o método resolveu o problema obtido pela divisão por 0 obtendo
o resultado correto. É possível ver também que a questão de achar um ponto crítico
distante do valor inicial que ocorria em x0 = 0.1 foi resolvido e em menos passos.

  Além disso, podemos notar que o exemplo que retornava um ponto de máximo, passou
a devolver o ponto de mínimo mais próximo, entretanto, com um custo adicional de passos
em comparação com o primeiro algoritmo.
#}

#{

Item 4: Método Quasi-Newton

Frequentemente a necessidade de conhecer as expressões analíticas das derivadas de f() pode ser
um empecilho à aplicação do método de Newton. Uma alternativa é usar aproximações, tais como

f'(x) ≈ (f(x+h)-f(x-h)) / (2h)
f"(x) ≈ (f'(x+h)-f'(x-h)) / (2h) ≈ (f(x+2h)-2*f(x)+f(x-2h)) / (4h²)

para algum h≠0 razoável. Isso poderia ser implementado no método de Newton usando h=x_atual-x_anterior.
Para isso seu método precisa de 2 pontos iniciais.

Adapte sua função do item 3 usando as aproximações das derivadas acima, e teste-a nos casos indicados,
comentando os resultados: o método falhou em algum caso? Como o número de iterações variou em relação ao item 3?

#}
function y = df_taylor(f, x, h)
  y = (f(x+h) - f(x-h))/(2*h);   
endfunction

function y = ddf_taylor(f, x, h)
  y = (f(x+(2*h)) - 2*f(x)+f(x-(2*h)))/(4*(h**2));   
endfunction

function [x,n,v,t] = Minimiza3(f,x0,x1,atol)
  n = 0;
  h = x1 - x0;
  x0 = x1;
  do
    df = df_taylor(@f, x0, h);
    ddf = ddf_taylor(@f, x0, h);
    if (abs(ddf) < eps || ddf < 0)
      x = x0 - df;
    else
      x = x0 - df/ddf;
    endif
    diff = abs(x - x0);
    h = x - x0;
    x0 = x;
    n++;
  until (diff < atol)
  v = f(x);
  t = sign(ddf);
endfunction

## códigos de teste
[x,n,v,t] = Minimiza3(@f,-0.1,0,1e-8) #-1.57, t = 1, n = 7
[x,n,v,t] = Minimiza3(@f,0,0.1,1e-8) #-1.57, t = 1, n = 7
[x,n,v,t] = Minimiza3(@f,0.5,1,1e-8) #-1.57, t = 1, n = 11
[x,n,v,t] = Minimiza3(@f,4,5,1e-8) #4.71, t = 1, n = 5

#{
Comentários:

  Temos que esse terceiro método gerou os mesmos resultados do segundo método,
entretanto, o tradeoff entre não passar as expressões analíticas das derivadas fez
com que o método precisasse de mais passos do que o método anterior justamente pelo
fator de erro associado a utilização da aproximação de Taylor em vez de um valor exato.
Porém, temos que o método não falhou em nenhum dos casos de teste.
#}

