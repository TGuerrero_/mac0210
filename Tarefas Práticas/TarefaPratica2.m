2;
#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Você deve submeter esse arquivo com esse mesmo nome 'quadsolver.m', no qual as primeiras linhas devem ser o cabeçalho preenchido e a definição da função.

Nome : Thiago Guerrero
NUSP : 11275297
Matéria: MAC0210
Turma: 2021
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 2 é escrever em Octave um resolvedor genérico de equações quadráticas da forma ax²+bx+c = 0
que seja numericamente robusto. Sua função deve ter a forma:

s = quadsolver(a,b,c)

onde os três parâmetros a, b, c são números reais em ponto flutuante e s é o vetor com todas as soluções reais e/ou complexas.
Se a equação não admitir solução, sua função deve devolver s = [], e se existirem infinitas soluções, sua função deve devolver
o código especial s = "R".

Você pode usar as equações de Bháskara s₁,₂ = (-b±sqrt(Δ))/(2a) com Δ=b²-4ac, bem como a relação s₁s₂=c/a, conforme a necessidade.
Tenha cuidado para evitar os erros de cancelamento (quando b≈±sqrt(Δ)) e de overflow (quando b² ou b/(2a) extrapolam o limite
representável de 10³⁰⁸), como explicado no exemplo 2.10, e lembre de corrigir as soluções para s₁,₂ = (-b±i*sqrt(|Δ|))/(2a) quando Δ<0. 

Alguns casos importantes de teste seguem abaixo; você pode usar o comando roots([a b c]) para reconfirmar as raízes esperadas
(a única diferença relevante ocorre no caso A, em que a resposta deve ser "R"):

A) quadsolver(0, 0, 0)
B) quadsolver(0, 0, 1)
C) quadsolver(0, 10^-300, 1)
D) quadsolver(10^-150, -10^150, 10^150)
E) quadsolver(10^-300, -10^300, 10^300)
F) quadsolver(1, -10^30, 1)
G) quadsolver(6*10^300, 5*10^300, -4*10^300)
H) quadsolver(10^300, 0, 10^300)

#}
function s = quadsolver(a,b,c)
  digits_a = finddigits(a); 
  digits_2a = finddigits(2*a);
  digits_b = finddigits(b);
  digits_c = finddigits(c);
  digits_2c = finddigits(2*c);

  if (((a != 0) && (a == b) && (b == c)) || abs(digits_b - digits_2a) > 308 || abs(digits_2c - digits_b) > 308)
    # Normalização dos fatores
    factor = max([a b c]);
    a = a/factor;
    b = b/factor;
    c = c/factor;
    digits_a = finddigits(a);
    digits_b = finddigits(b);
    digits_c = finddigits(c);
  endif

  if (a == 0)
    if (b == 0)
      if (c == 0)
        s = "R";
      else
        s = [];
      endif
    else
      s = -c/b;
    endif
  else
    delta = b*b - (4*a*c);
    if (digits_b > 150)
      # Normalização do b²
      sqrt_delta = abs(b)*sqrt(1 - 4*(a/b)*(c/b));
    elseif (digits_a > 150 && digits_c > 150)
      # Normalização do 4ac
      sqrt_delta = 2*sqrt(a)*sqrt(c)*sqrt(abs((b**2)/(4*a*c) - 1));
    else
      # b² e 4ac não causam overflow
      sqrt_delta = sqrt(abs(delta));
    endif
    s = findroots(a, b, c, delta, sqrt_delta);
  endif
endfunction

function digits = finddigits(number)
  digits = 0;
  if (number)
    digits = log10(abs(number));
  endif
endfunction

function root = findroots(a, b, c, delta, sqrt_delta)
  imaginary_root = false;
  if (delta < 0)
    imaginary_root = true;
  endif

  if (delta == 0)
    s12 = -b/(2*a);
    root = [s12 s12];
  elseif (sqrt_delta == abs(b))
    # Erro de cancelamento
    if (b > 0)
      if (imaginary_root)
        s2 = (-b-i*sqrt_delta)/(2*a);
      else
        s2 = (-b-sqrt_delta)/(2*a);
      endif
      root = [c/(a*s2) s2];
    else
      if (imaginary_root)
        s1 = (-b+i*sqrt_delta)/(2*a);
      else
        s1 = (-b+sqrt_delta)/(2*a);
      endif
      root = [s1 c/(a*s1)];
    endif
  else
    # Não ocorre cancelamento
    if (imaginary_root)
      root = [(-b+i*sqrt_delta)/(2*a) (-b-i*sqrt_delta)/(2*a)];
    else
      root = [(-b+sqrt_delta)/(2*a) (-b-sqrt_delta)/(2*a)];
    endif
  endif
endfunction

#Ground Truth
display("Função roots");
a = roots([0 0 0])
b = roots([0 0 1])
c = roots([0 10^-300 1])
d = roots([10^-150 -10^150 10^150])
e = roots([10^-300 -10^300 10^300])
f = roots([1 -10^30 1])
g = roots([6*10^300 5*10^300 -4*10^300])
h = roots([10^300 0 10^300])

printf("\n");
display("Função Quadsolver");
a = quadsolver(0, 0, 0)
b = quadsolver(0, 0, 1)
c = quadsolver(0, 10^-300, 1)
d = quadsolver(10^-150, -10^150, 10^150)
e = quadsolver(10^-300, -10^300, 10^300)
f = quadsolver(1, -10^30, 1)
g = quadsolver(6*10^300, 5*10^300, -4*10^300)
h = quadsolver(10^300, 0, 10^300)

% ground = roots([10^300, 10^300, 10^300])
% quadsolver(10^300, 10^300, 10^300)
% ground = roots([10^-300, 10^300, 10^300])
% quadsolver(10^-300, 10^300, 10^300)
% ground = roots([10^300, 10^-300, 10^300])
% quadsolver(10^300, 10^-300, 10^300)
% ground = roots([10^-300, 10^-300, 10^300])
% quadsolver(10^-300, 10^-300, 10^300)
% ground = roots([10^300, 10^300, 10^-300])
% quadsolver(10^300, 10^300, 10^-300)
% ground = roots([10^-300, 10^300, 10^-300])
% quadsolver(10^-300, 10^300, 10^-300)
% ground = roots([10^300, 10^-300, 10^-300])
% quadsolver(10^300, 10^-300, 10^-300)
% ground = roots([10^-300, 10^-300, 10^-300])
% quadsolver(10^-300, 10^-300, 10^-300)