#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Nome : Thiago Guerrero
NUSP : 11275297
Matéria: MAC0210
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 5 é entendermos como o erro global associado à interpolação
polinomial se comporta em função da quantidade e das posições dos pontos xi onde a função é
observada, e explorarmos uma abordagem local para a construção de funções interpoladoras, que
evita os erros associados ao uso de polinômios de grau muito alto.

Para realizar essa tarefa, baixe o arquivo lagrangeinterp.m com a função 

function y = lagrangeinterp(x,xi,yi)

que calcula o polinômio interpolador associado às observações (xi,yi)
nos pontos x, usando a versão baricêntrica da interpolação de Lagrange.

A tarefa está organizada em 4 itens, descritos abaixo.

#}

0; # força o Octave a interpretar esse arquivo como um script file (ver https://octave.org/doc/v5.2.0/Script-Files.html)

#{

Item 1 (parcialmente resolvido no enunciado):

Considere uma função f(x) no intervalo x∈[-1,1] (ainda não definida) que será aproximada
usando um polinômio interpolador p(x) construído a partir de n+1 observações localizadas em
xi = -1:2/n:1. Pelo Teorema do erro da aproximação polinomial, sabemos que a diferença
e(x)=f(x)-p(x) pode ser escrita como

e(x) = f^{n+1}(ξ)·ψ(x,xi)/(n+1)!

sendo que a função

ψ(x,xi) = Π(x-xi) = (x-x0)(x-x1)···(x-xn)

não depende de f(x), mas apenas das abcissas xi das observações.

Plote os gráficos de ψ(x) para n=4,10,50,100 e x = -1:1e-3:1. Plote também os valores
absolutos máximos (max(abs(ψ)) em função de n (usando a função semilogy()) e responda
as seguintes perguntas:

1-A) Como se distribuem os valores de psi no domínio x∈[-1,1], e onde se concentram os maiores valores dessas funções quando n aumenta?
  Podemos notar que entre os intervalos [x_1, x_2] e [x_n, x_{n+1}] os valores de psi acabam estourando
 em relação aos demais valores, entretanto, esses valores estão cada vez menores com o aumento
 do n. Logo, conseguimos ver que os valores de psi estão se concentrando cada vez mais perto
 do valor 0.

1-B) Como os valores absolutos máximos de ψ(x,xi) se comportam em função de n?
  Conseguimos notar que o valor máximo de psi diminui quase exponencialmente com o aumento
 do valor de n. Podemos ver, por exemplo, que o valor de m para n = 50 é aproximadamente
 10^-8 enquanto o valor de m para n = 100 está bem próximo de 10^-16.
#}

## Função ψ(x,xi):
function y = psi(x,xi)
  y = ones(1,length(x));
  for i = 1:length(xi)
    y .*= (x-xi(i));
  endfor
endfunction

## domínio para todos os gráficos em função de x:
x = -1:1e-3:1;

## valores de n considerados
N = [4, 10, 50, 100];

## plota as funções ψ(x,xi) para n∈N
close all; ## fecha figuras que estiverem abertas
m = []; ## guarda valores de psi máximo
for n = N
  xi = -1:2/n:1; ## posições (abcissas) de observação
  psix = psi(x,xi); ## função ψ(x,xi)
  m = [ m max(abs(psix))]; ## psi máximo
  figure();
  plot(x,psix, xi,psi(xi,xi),"*"); ## plota ψ(x), marcando com '*' as observações
endfor
figure();
semilogy(N,m);
title("Erro máximo");
xlabel(n);
ylabel("psi(x)");

#{

Item 2:

Use a chamada pn = lagrangeinterp(x,xi,yi) para computar as aproximações polinomiais pn(x) da função

f(x) = 1/(1+25x²), x∈[-1,1]

usando as observações xi = -1:2/n:1 e yi = 1./(1+25*xi.^2) para n=4,10,50,100,150,200.
Compute também o erro absoluto máximo emax(n) = max(abs(f(x)-pn(x))) para cada valor de n.

Para cada n, plote a aproximação polinomial ao lado da função original
(acrescente também ',xi,yi,"*"' ao comando plot para visualizar as observações usadas na interpolação).
Plote também o erro máximo em função de n (usando semilogy), e comente sobre a qualidade das aproximações
à medida que o número de observações aumenta. Em particular, na fórmula do erro da interpolação polinomial
apresentada no item 1, qual fator pode explicar suas observações?

Dica: aproveite a estrutura do código do item 1.

#}

function y = f(x)
  y = 1./(1+(25*x.**2));
endfunction

close all;
emax = [];
for n = N
  xi = -1:2/n:1; ## posições (abcissas) de observação
  yi = f(xi);
  y = f(x);
  pn = lagrangeinterp(x, xi, yi);
  emax = [emax max(abs(y - pn))];
  figure();
  plot(x, y, "r", x, pn, "--g", xi, yi, "*");
  title(["N = " num2str(n)]);
  legend("f(x)", "p(x)", "xi", "location", "south");
endfor
figure();
semilogy(N, emax, "k");
title("Erro máximo");
xlabel("n");
ylabel("e(x)");

#{
  Podemos ver que com o aumento do n, menor é o erro observado na maioria dos pontos do domínio.
 Entretanto, conseguimos observar que em alguns pontos o polinômio interpolador apresenta um
 comportamento não razoável e que se torna cada vez pior com o aumento do n, fazendo com que o
 erro máximo tenda cada vez mais para valores maiores.
 
  Analisando a expressão do erro dada por e(x) = f^{n+1}(ξ)·ψ(x,xi)/(n+1)!, podemos concluir que
 o fator responsável pelo aumento no erro máximo é o termo f^{n+1}, pois o denominador será sempre
 crescente e, como vimos no item 1, a função psi tem o seu valor máximo sempre reduzido no intervalo
 especificado para o n. De fato, se olharmos para a expressão da função f, podemos ver que o grau
 do polinômio aumenta conforme derivamos, fazendo com que o valor tenda a ser cada vez maior em 
 alguns pontos.
#}

#{

Item 3:
Para observarmos a dependência entre o perfil da função ψ(x) e a posição dos pontos xi,
considere as seguintes variantes para a posição destas abcissas:

## amostras mais concentradas no centro (variante 1)
xi = -1:2/n:1; xi = sign(xi).*xi.^2;

## amostras mais concentradas nas pontas (variante 2)
xi = -1:2/n:1; xi = sign(xi).*sqrt(abs(xi));

Repita o mesmo código que você escreveu no item 2 com essas duas variantes para os pontos
xi, e comente em seguida sobre a qualidade das aproximações obtidas, comparando-as com os
resultados do item 2. Você observa alguma relação entre a melhor distribuição dos pontos xi
e os perfis das funções ψ(x) do item 1?

#}

close all;
emax_1 = [];
emax_2 = [];
for n = N
  xi = -1:2/n:1;
  xi_1 = sign(xi).*xi.^2;
  xi_2 = sign(xi).*sqrt(abs(xi));
  
  yi_1 = f(xi_1);
  yi_2 = f(xi_2);
  y = f(x);
  
  pn_1 = lagrangeinterp(x, xi_1, yi_1);
  pn_2 = lagrangeinterp(x, xi_2, yi_2);
  
  emax_1 = [emax_1 max(abs(y - pn_1))];
  emax_2 = [emax_2 max(abs(y - pn_2))];
  figure();
  plot(x, y, "r", x, pn_1, "--g", xi_1, yi_1, "*");
  title(["Variante 1. n = " num2str(n)]);
  legend("f(x)", "p(x)", "xi", "location", "south");
  figure();
  plot(x, y, "r", x, pn_2, "--g", xi_2, yi_2, "*");
  title(["Variante 2. n = " num2str(n)]);
  legend("f(x)", "p(x)", "xi", "location", "south");
endfor
figure();
semilogy(N, emax_1, "r", N, emax_2, "g");
legend("Variante 1", "Variante 2", "location", "north");
title("Erro máximo");
xlabel("n");
ylabel("e(x)");

#{
  Analisando a primeira variante, conseguimos ver que a partir de N = 50 ela possui um 
 comportamento mais razoável em relação aos pontos que estouram com o método dos pontos 
 equidistantes. Antes disso ela possui um comportamento pior em relação ao outro método. Se
 compararmos o valor max de ψ(x,xi) para a primeira variante, vamos perceber que os
 valores são maiores  do que os observados no item 1. Entretanto, como a escolha dos
 valores de xi também afeta o valor de ξ usado no termo f^{n+1}(ξ), podemos deduzir que
 a partir de N = 50 o termo f^{n+1}(ξ) acaba compensando esse aumento no valor de ψ, gerando
 um polinômio com menores erros.
 
  Já com a segunda variante temos, desde o início, que ela apresenta um comportamento 
 mais razoável do que o observado no item 2 e na primeira variante. Analisando os valores max
 de ψ(x,xi) para a segunda variante, podemos notar que concentrar as observações nas bordas do
 intervalo ajuda a controlar o comportamento que ocorre no extremo do intervalo de observações 
 notado no item 1. Assim, fazendo com que os valores max de ψ da segunda variante sejam os menores
 e produzam polinômios com menores erros.
#}

#{

Item 4:

Uma abordagem diferente para melhorar a qualidade de aproximações polinomiais com muitas
observações consiste em interpolar pequenos trechos da função utilizando polinômios de
grau baixo. Você poderia por exemplo considerar polinômios cúbicos construídos a partir
de 4 observações (x1,y1),(x2,y2),(x3,y3),(x4,y4) para aproximar apenas o trecho [x2,x3],
e "costurar" as aproximações de todos os trechos.

Por exemplo, usando a função lagrangeinterp podemos aproximar o trecho [x2,x3] fazendo:
indices = lookup(x,xi(2)):lookup(x,xi(3));
y(indices) = lagrangeinterp(x(indices),xi(1:4),yi(1:4));

Adapte sua solução do item 2 (com os pontos xi = -1:2/n:1), aplicando essa estratégia a
cada trecho x(i):x(i+1) para i=2,...,n-1 (desconsidere os intervalos [x1,x2] e [xn,xn+1]).
Calcule o erro absoluto máximo da aproximação obtida, para n=4,10,50,100.
Como esse erro se relaciona com os dos itens anteriores?

#}

close all;
emax_3 = [];
for n = N
  xi = -1:2/n:1;
  x = xi(2):1e-3:xi(n);
  yi = f(xi);
  y = f(x);
  
  pn = zeros(1, length(x));
  for i = 2:n-1
    indices = lookup(x, xi(i)):lookup(x, xi(i+1));
    pn(indices) = lagrangeinterp(x(indices), xi(i-1:i+2), yi(i-1:i+2));
  endfor

  emax_3 = [emax_3 max(abs(y - pn))];
  figure();
  plot(x, y, "r", x, pn, "--g", xi(2:n), yi(2:n), "*");
  title(["n = " num2str(n)]);
  legend("f(x)", "p(x)", "xi", "location", "south");
endfor
figure();
semilogy(N, emax_3, "b");
title("Erro máximo");
xlabel("n");
ylabel("e(x)");

# Adicional
close all;
figure();
semilogy(N, emax, "k", N, emax_1, "r", N, emax_2, "g", N, emax_3, "b");
title("Erro máximo de todos os métodos");
xlabel("n");
ylabel("e(x)");
legend("Equidistante", "Variante 1", "Variante 2", "Interpolação por trechos", "location", "northwest");


#{
  Analisando os gráficos, conseguimos perceber que a distância do polinômio interpolador
 até o gráfico da função fica menor usando essa interpolação em comparação com os
 outros métodos, em específico, em comparação com a variante 2 do item anterior, que até
 então era o melhor método. Olhando o gráfico do erro máximo conseguimos ver que o erro
 da variante 2 nunca fica abaixo de 10^-2, já o erro dessa interpolação atinge valores 
 abaixo de 10^-2 já para N = 50, diminuindo ainda mais para os outros tamanhos.
#}

## Dica: substitua apenas a parte que calcula pn(x), usando os valores de x entre xi(2) e xi(n):
## x = xi(2):1e-3:xi(n)



