function y = lagrangeinterp(x,xi,yi)
  ## calcula o polinômio interpolador associado às observações
  ## (xi,yi) nos pontos x, usando a versão baricêntrica da
  ## interpolação de Lagrange.
  ## 1) calcula os pesos baricêntricos p(i) = 1/Π(xi-xj) (j≠i)
  p = ones(1,length(xi));
  for i = 1:length(p)
    for j = [1:i-1 i+1:length(p)]
      p(i) /= (xi(i)-xi(j));
    endfor
  endfor
  ## 2) calcula para cada x(j) o valor y(j) do polinômio interpolador
  y = zeros(1,length(x));
  for j = 1:length(x)
    ## calcula numerador = Σ yi*p(i)/(x-xi) e denominador = Σ p(i)/(x-xi)
    num = den = 0;
    flag = 0; ## usado para identificar se x=xi para algum i
    for i = 1:length(xi)
      if abs(x(j)-xi(i))<eps ## se x(j)=xi, então y(j) deve ser yi
        y(j) = yi(i);
        flag = 1; ## força a interrupção do laço
        break;
      endif
      temp = p(i)/(x(j)-xi(i)); ## termo comum ao numerador e denominador
      num += yi(i)*temp;
      den += temp;
    endfor
    if flag == 0 ## se o laço anterior terminou normalmente
      y(j) = num/den; ## aplica a fórmula baricêntrica
    endif
  endfor
endfunction
