#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Nome : Thiago Guerrero
NUSP : 11275297
Matéria: MAC0210
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 4 é explorar a base de polinômios utilizada na interpolação
de Newton, cuja principal característica é incorporar informações sobre cada novo ponto interpolado
de forma incremental. Essa base é construída a partir de uma sequência de informações (xi,yi), i=0,1,2,...,
onde yi=f(xi), como segue:

N0(x) = 1
N1(x) = (x-x0)
N2(x) = (x-x0)(x-x1)
N3(x) = (x-x0)(x-x1)(x-x2)
.
.
.
N{k+1}(x) = (x-x0)(x-x1)(x-x2)···(x-xk)
.
.
.

ou equivalentemente, de forma recursiva:

N0(x) = 1
N{k+1}(x) = Nk(x)(x-xk), k≥0.

Observe que, por construção, N{k+1}(x) = 0 para x = x0, x1, ...,xk.

A interpolação dos pontos (x0,y0),...,(xn,yn) é definida pelo polinômio p(x) = Σ ci·Ni(x) que satisfaz as condições

p(xi) = yi, i=0,...,n.

A tarefa está organizada em 4 itens, descritos abaixo.

#}

0; # força o Octave a interpretar esse arquivo como um script file (ver https://octave.org/doc/v5.2.0/Script-Files.html)

# função auxiliar para colocar os eixos na origem:
function eixosnaorigem()
  set(gca, "box", "off");
  set (gca, "xaxislocation", "origin");
  set (gca, "yaxislocation", "origin");
endfunction

#{

Considere os pontos dados (-1,-1), (0,0), (2,8) e (1,1) — nessa ordem — a serem interpolados
com a base de Newton de forma incremental. Em cada passo k, plotaremos cada componente ci·Ni(x), i=0,...,k,
bem como a soma p(x) = Σ_{i<k} ci·Ni(x) das componentes definidas até aquele passo, para x no intervalo x_ = -2:0.1:3.

#}

## domínio das funções
x_ = -2:0.1:3

## pontos dados
x = [-1 0 2 1];
y = [-1 0 8 1];

#{

Item 1 (feito em aula):

Construa o polinômio interpolador de Newton para o ponto (-1,-1), indicando as condições
a serem satisfeitas para o cálculo de c0, e plote a função p0(x) = c0N0(x), além de todos os
pontos dados (use para isso plot(x,y,"*",...) e a função eixosnaorigem() definida acima).

#}

#{
  interpolação do ponto (-1,-1) com o polinômio N0(x)=1
  a única condição a ser satisfeita é
  p(x0) = c0·N0(x0) = y0 ==> c0 = y0
#}
c0 = y(1)
N0 = ones(1,length(x_));
p0 = c0*N0;
figure(1);
plot(x,y,"*",x_,p0);
legend("Observações", "p0", "location", "north")
eixosnaorigem();

#{

Item 2 (feito em aula):

Construa o polinômio interpolador de Newton para os pontos (-1,-1) e (0,0), plotando
as componentes p0(x) = c0N0(x) e p1(x) = c1N1(x), sua soma p(x) = p0(x)+p1(x), além de
todos os pontos dados.

Para cada função plotada indique quais pontos ela interpola e/ou se ela possui raízes
em algum dos pontos dados.

Escreva também a expressão de p(x) na forma usual (usando a base dos monômios).

#}

#{ 
  interpolação dos pontos (-1,-1) e (0,0) com os polinômios
  N0(x)=1 e N1(x) = (x-x0)
  as condições a serem satisfeitas são
  p(x0) = c0·N0(x0)+c1·N1(x0) = y0
  p(x1) = c0·N0(x1)+c1·N1(x1) = y1
  observe que N1(x0) = 0, de onde
  p(x0) = c0·N0(x0)           = y0
  p(x1) = c0·N0(x1)+c1·N1(x1) = y1
  tornando a primeira condição idêntica à anterior
           c0·N0(x0) = y0 ==> c0 = y0
  e consequentemente a primeira componente p0 também;
  a segunda condição fica
  c0·N0(x1)+c1·N1(x1) = y1 ==> y0+c1·(x1-x0) = y1
  ==> c1 = (y1-c0)/(x1-x0) = (y1-y0)/(x1-x0)
#}
c1 = (y(2)-c0)/(x(2)-x(1))
N1 = (x_-x(1));
p1 = c1*N1;
figure(2);
plot(x,y,"*",x_,p0,x_,p1,x_,p0+p1);
legend("Observações", "p0", "p1", "p0+p1", "location", "north")
eixosnaorigem();

#{
  observamos que a primeira componente interpola
  o ponto (x0,y0) (p0(x0)=y0), e que a segunda
  componente p1(x) possui uma raiz em x0 (p1(x0)=0),
  o que garante que a soma das duas componentes
  não deixará de interpolar o ponto (x0,y0)
  (pois p(x0)=p0(x0)+p1(x0)=y0+0=y0). A soma p(x)
  interpola os dois pontos (x0,y0) e (x1,y1), como
  já era de se esperar.

  a expressão de p(x) pode ser escrita usando os valores
  de c0=-1 e c1=1 e as expressões dos polinômios de
  Newton, substituindo os valores de x0=-1 e x1=0:

  p(x) = c0N0(x)+c1N1(x) = -1·1+1·(x-x0) = -1+(x-(-1)) = x
  
  que corresponde à imagem plotada e de fato interpola
  os pontos (-1,-1) e (0,0).

  Construindo o polinômio p(x) pela base dos monômios, temos que:
  p(x0) = c0 + c1*x0 = y0
  p(x1) = c0 + c1*x1 = y1
  
  Usando as observações, temos:
  p(-1) = c0 - c1 = -1
  p(0) = c0 = 0
  
  Logo, c0 = 0 e c1 = 1, então:
  p(x) = x
#}


#{

Item 3 (dica: aproveite a estrutura da resposta anterior!):

Construa o polinômio interpolador de Newton para os pontos (-1,-1), (0,0) e (2,8),
plotando as componentes

    pi(x) = ciNi(x), i=0,1,2,

suas somas parciais

    p0(x)+p1(x),
    p(x) = p0(x)+p1(x)+p2(x),

além de todos os pontos dados.

Para cada função plotada indique quais pontos ela interpola e/ou se ela possui raízes
em algum dos pontos dados.

Escreva também a expressão de p(x) na forma usual (usando a base dos monômios).

#}

#{
  interpolação dos pontos (-1,-1), (0,0) e (2, 8) com os polinômios
  N0(x)=1, N1(x) = (x-x0) e N2(x) = (x - x0)*(x - x1)
  Logo, temos:
  p(x0) = c0*N0(x0)+c1*N1(x0)+c2*N2(x0) = y0
  p(x1) = c0*N0(x1)+c1*N1(x1)+c2*N2(x1) = y1
  p(x2) = c0*N0(x2)+c1*N1(x2)+c2*N2(x2) = y2
  
  Como N1(x0) = N2(x0) = N2(x1) = 0 e N0(x) = 1, temos:
  p(x0) = c0 = y0 => c0 = -1
  p(x1) = c0+c1*N1(x1) = y1 => c1 = (y1 - y0)/(x1 - x0) = -1/-1 = 1
  p(x2) = c0+c1*N1(x2)+c2*N2(x2) = y2 => c2 = y2 - c0 - c1*(x2 - x0))/((x2 - x0)*(x2 - x1)) = 6/6 = 1
#}
c2 = (y(3) - c0 - c1*(x(3) - x(1)))/((x(3) - x(1)) * (x(3) - x(2)))
N2 = (x_ - x(1)).*(x_ - x(2));
p2 = c2*N2;
figure(3);
plot(x, y, "*", x_, p0, x_, p1, x_, p2);
hold on
plot(x_, p0+p1, x_, p0+p1+p2);
legend("Observações", "p0", "p1", "p2", "p0+p1", "p0+p1+p2", "location", "north")
eixosnaorigem();
#{

#{
  Podemos notar as mesmas observações que acontecem no item anterior e, além disso,
  observar que o componente p2 possui uma raiz em x0 e uma raiz em x1, o que garante que
  a soma das três componentes não deixará de interpolar o ponto (x0, y0) (pois 
  p(x0) = p0(x0)+p1(x0)+p2(x0) = y0 + 0 + 0 = y0) e nem o ponto (x1, y1) (pois
  p(x1) = p0(x1)+p1(x1)+p2(x1) =x0 + x1-x0 + 0 = x1 = y1). A soma p(x) interpola
  os três pontos (x0, y0), (x1, y1) e (x2, y2) como esperávamos.

  Construindo o polinômio p(x) pela base dos monômios, temos que:
  p(x0) = c0 + c1*x0 + c2*x0^2 = y0
  p(x1) = c0 + c1*x1 + c2*x1^2 = y1
  p(x2) = c0 + c1*x2 + c2*x2^2 = y2
  
  Usando as observações, temos:
  p(-1) = c0 - c1 + c2 = -1
  p(0) = c0 = 0
  p(2) = c0 + 2*c1 + 4*c2 = 8

  
  Logo, c0 = 0, c1 = 2, c2 = 1
  p(x) = x^2 + 2x
#}

Item 4:

Construa o polinômio interpolador de Newton para os pontos (-1,-1), (0,0), (2,8) e (1,1),
plotando as componentes

    pi(x) = ciNi(x), i=0,1,2,

suas somas parciais

    p0(x)+p1(x),
    p0(x)+p1(x)+p2(x),
    p(x) = p0(x)+p1(x)+p2(x)+p3(x),

além de todos os pontos dados.

Para cada função plotada indique quais pontos ela interpola e/ou se ela possui raízes
em algum dos pontos dados.

Escreva também a expressão de p(x) na forma usual (usando a base dos monômios).

#} 
#{
  interpolação dos pontos (-1,-1), (0,0), (2, 8), (1, 1) com os polinômios
  N0(x)=1, N1(x) = (x - x0), N2(x) = N1*(x - x1) e N3 = N2*(x - x2)
  Logo, temos:
  p(x0) = c0*N0(x0)+c1*N1(x0)+c2*N2(x0)+c3*N3(x0) = y0
  p(x1) = c0*N0(x1)+c1*N1(x1)+c2*N2(x1)+c3*N3(x1) = y1
  p(x2) = c0*N0(x2)+c1*N1(x2)+c2*N2(x2)+c3*N3(x2) = y2
  p(x3) = c0*N0(x3)+c1*N1(x3)+c2*N2(x3)+c3*N3(x3) = y3
  
  Como N1(x0) = N2(x0) = N2(x1) = N3(x0) = N3(x1) = N3(x2) = 0 e N0(x) = 1, temos:
  p(x0) = c0 = y0 => c0 = -1
  p(x1) = c0+c1*N1(x1) = y1 => c1 = (y1 - y0)/(x1 - x0) = -1/-1 = 1
  p(x2) = c0+c1*N1(x2)+c2*N2(x2) = y2 => c2 = y2 - c0 - c1*(x2 - x0))/((x2 - x0)*(x2 - x1)) = 6/6 = 1
  p(x3) = c0+c1*N1(x3)+c2*N2(x3)+c3*N3(x3) = y3 => c3 = (y3 - c0 - c1*(x3 - x0) - C2*(x3 - x0)*(x3 - x1))/((x3 - x0)*(x3 - x1)*(x3 - x2)) = -2/-2 = 1
#}
c3 = (y(4) - c0 - c1*(x(4) - x(1)) - c2*(x(4) - x(1))*(x(4) - x(2)))/((x(4) - x(1))*(x(4) - x(2))*(x(4) - x(3)))
N3 = (x_ - x(1)).*(x_ - x(2)).*(x_ - x(3));
p3 = c3*N3;
figure(4);
plot(x, y, "*", x_, p0, x_, p1, x_, p2, x_, p3);
hold on
plot(x_, p0+p1, x_, p0+p1+p2, x_, p0+p1+p2+p3);
legend("Observações", "p0", "p1", "p2", "p3", "p0+p1", "p0+p1+p2", "p0+p1+p2+p3", "location", "north")
eixosnaorigem();

#{
  Podemos notar as mesmas observações que acontecem nos items anteriores e, além disso,
  observar que o componente p3 possui uma raiz em x0, x1 e uma raiz em x2, o que garante que
  a soma das quatro componentes não deixará de interpolar o ponto (x0, y0) (pois 
  p(x0) = p0(x0)+p1(x0)+p2(x0)+p3(x0) = y0 + 0 + 0 + 0 = y0), nem o ponto (x1, y1) (pois
  p(x1) = p0(x1)+p1(x1)+p2(x1)+p3(x1) =x0 + x1-x0 + 0 + 0 = x1 = y1) e nem o ponto (x2, y2)
  (pois p(x2) = p0(x2)+p1(x2)+p2(x2)+p3(x2) = x0 + x2-x0 + (x2-x0)*(x2-x1) + 0 = -1 + 3 + 6 + 0 = 8 = y2). 
  A soma p(x) interpola os quatro pontos (x0, y0), (x1, y1), (x2, y2) e (x3, y3) como esperávamos.

  Construindo o polinômio p(x) pela base dos monômios, temos que:
  p(x0) = c0 + c1*x0 + c2*x0^2 + c3*x0^3 = y0
  p(x1) = c0 + c1*x1 + c2*x1^2 + c3*x1^3 = y1
  p(x2) = c0 + c1*x2 + c2*x2^2 + c3*x2^3 = y2
  p(x3) = c0 + c1*x3 + c2*x3^2 + c3*x3^3 = y3
  
  Usando as observações, temos:
  p(-1) = c0 - c1 + c2 - c3 = -1
  p(0) = c0 = 0
  p(2) = c0 + 2*c1 + 4*c2 + 8*c3 = 8
  p(3) = c0 + c1 + c2 + c3 = 1
  
  Logo, c0 = 0, c1 = 0, c2 = 0, c3 = 1
  p(x) = x^3
#}

