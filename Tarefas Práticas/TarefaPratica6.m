#{

PREENCHA O CABEÇALHO ABAIXO. TPS SEM IDENTIFICAÇÃO NÃO SERÃO CORRIGIDAS.

Nome : Thiago Guerrero
NUSP : 11275297
Matéria: MAC0210
Prof.: Marcelo Queiroz

O objetivo dessa Tarefa Prática 6 é entendermos como a interpolação polinomial pode
servir para o problema de diferenciação numérica, ou seja, calcular f'(x0) usando
apenas chamadas da função original f(·).

Considerando a interpolação de Lagrange a partir de pontos da forma xj = x0+jh, com
j inteiro, podemos escrever o polinômio aproximador

p(x) = Σ_{j} f(xj)Lj(x)

onde Lj(x) =  Π_{k≠j} (x-xk) 
             -----------------.
              Π_{k≠j} (xj-xk) 

Daí podemos escrever uma aproximação da derivada f'(x0) como

p'(x0) = Σ_{j} f(xj)L'j(x0)

onde L'j(x0) pode ser calculado diretamente através das seguintes expressões
(verifique-as depois de entregar a tarefa):

L'0(x0) = -(1/h)*(Σ_{k≠0} 1/k)

L'j(x0) = (1/(jh))*(Π_{k≠0,j} k/(k-j))

Veremos como utilizar essas fórmulas para construir estimativas da derivada f'(x0)
com acurácia associada ao número de pontos usados no polinômio interpolador, bem
como ao espaçamento h entre esses pontos.

Essa tarefa está organizada em 4 itens, descritos abaixo.

#}

0; # força o Octave a interpretar esse arquivo como um script file (ver https://octave.org/doc/v5.2.0/Script-Files.html)
clear all;
close all;

#{

Item 1: Derivada usando 2 e 3 pontos

Considerando apenas os pontos x0  e x1=x0+h, temos a estimativa

p2'(x0) = f(x0)L'0(x0)+f(x1)L'1(x0)

onde

L'0(x0) = -(1/h)*(Σ_{k≠0} 1/k) = -1/h
L'1(x0) = (1/(1h))*(Π_{k≠0,1} k/(k-1)) = 1/h (obs: produtório vazio = 1)

de onde

p2'(x) = f(x0)/(-h) + f(x1)/h = (f(x0+h)-f(x0))/h

Essa fórmula é nossa velha conhecida do capítulo 1!

Se usarmos 3 pontos x_1=x0-h, x0 e x1=x0+h, teremos

p3'(x0) = f(x_1)L'_1(x0)+f(x0)L'0(x0)+f(x1)L'1(x0)

onde

L'_1(x0) = (1/(-1h))*(Π_{k≠0,-1} k/(k+1)) = -1/(2h)
L'0(x0) = -(1/h)*(Σ_{k≠0} 1/k) = -(1/h)*(-1+1) = 0
L'1(x0) = (1/(1h))*(Π_{k≠0,1} k/(k-1)) = 1/(2h)

de onde

p3'(x0) = (f(x1)-f(x_1))/(2h)

outra fórmula conhecida desde o capítulo 1 (note que o x0 nem é usado, pois L'0(x0)=0).

Rode os códigos abaixo para produzir os erros de aproximação da derivada de f(x)=e^x
em x0 = 0 em função de h=1e-1, 1e-2, ..., 1e-16 (lembre que f'(x)=e^x=f(x)) e observe os gráficos.
Como as ordens de grandeza (expoentes) dos erros se comportam em função da ordem de grandeza do h?
Onde ocorrem os pontos de "quebra" de cada método, onde os erros da discretização passam a ser 
dominados pelos erros de arredondamento?

Estime a partir dos resultados a expressão do erro teórico f'(x0)-p'(x0)≈β·h^k em função de h
para as duas aproximações, considerando os primeiros valores de h (os mais altos, onde o erro é
mais bem comportado). Quem são β e k nos dois casos?

Dica: o quociente entre os erros associados a valores sucessivos de h, por exemplo (exp(0)-y(2))/(exp(0)-y(1))
ou (exp(0)-w(2))/(exp(0)-w(1)), permite estimar o inteiro k>0; por exemplo, um método O(h) tem o erro
divido por 10 quando h é dividido por 10, um método O(h²) divide o erro por 100 cada vez que h é dividido
por 10, e assim por diante. Depois disso, dividir os erros por h^k permite estimar o valor de β
(você pode tomar a média dos valores "confiáveis" de β, que são os valores similares obtidos nas primeiras estimativas).

#}


## gabarito parcial:

## derivada usando 2 pontos x0 e x0+h:
function y = derivada2(f,x0,h)
  y = (f(x0+h)-f(x0))./h;
endfunction

## derivada usando 3 pontos x0-h, x0 e x0+h:
function y = derivada3(f,x0,h)
  y = (f(x0+h)-f(x0-h))./(2*h);
endfunction

## valores de h para o teste
h = 10.^(-1:-1:-16);

## calcula as duas estimativas para
## a derivada da função e^x em x0=0
figure(1);
hold on;
y = derivada2(@exp,0,h);
loglog(h,abs(exp(0)-y));
w = derivada3(@exp,0,h);
loglog(h,abs(exp(0)-w));
xlabel("h");
ylabel("Erro");
legend("2 pontos", "3 pontos", "location", "north");
#{
  Podemos ver, usando 2 pontos, que a "quebra" ocorre em 10^-8 e que a ordem de grandeza
 do erro parece reduzir de forma linear em relação à ordem de grandeza do h (O(h)).
    
  Para 3 pontos, temos que a "quebra" ocorre antes, em 10^-5 e que a ordem de grandeza
 do erro parece reduzir de forma quadrática em relação à ordem de grandeza do h (O(h^2)).
#}

k = 0;
for i = 1:7
  k += abs((exp(0)-y(i+1))/(exp(0)-y(i)));
endfor
display("\nValor de k arredondado para 2 pontos:");
k = round(abs(log10(k/7)))
display("Valor de beta para 2 pontos:");
beta = mean(abs(exp(0) - y(1:8))/h(1:8).^k)
#{
  Olhando somente para o intervalo de valores em que o método usando 2 pontos não
 estoura ([10^-8, 10^-1]), conseguimos aproximar o valor de k e beta como sendo,
 respectivamente, 1 e 0.51694, como mostra o código acima.
#}

k = 0;
for i = 1:4
  k += abs((exp(0)-w(i+1))/(exp(0)-w(i)));
endfor
display("\nValor de k arredondado para 3 pontos:");
k = round(abs(log10(k/4)))
display("Valor de beta para 3 pontos:");
beta = mean(abs(exp(0) - w(1:5))/h(1:5).^k)
#{
  Olhando somente para o intervalo de valores em que o método usando 3 pontos não
 estoura ([10^-5, 10^-1]), conseguimos aproximar o valor de k e beta como sendo,
 respectivamente, 2 e 0.16675, como mostra o código acima.
#}

#{

Item 2:

Aplique as fórmulas do início do enunciado para deduzir uma expressão para a derivada
envolvendo a avaliação de f(x) nos pontos x_2=x0-2h, x_1=x0-h, x0, x1=x0+h e x2=x0+2h.
Indique suas contas em comentários no código, e implemente uma função

function y = derivada5(f,x0,h)

que calcule essa estimativa. Depois disso, aplique sua função aos mesmos dados do item 1,
inclua o gráfico dela no plot anterior, e estime os parâmetros k e β para essa aproximação.

Dicas: Aproveite parte do código do item 1. Os valores de L'j(x0) serão ±1/12 e ±2/3,
não necessariamente nessa ordem. Para a estimativa do k, considere apenas o primeiro
quociente de erros.

#}

#{
  L'-2(x0) = -1/2h*-1*1/3*1/2 = 1/12h
  L'-1(x0) = -1/h*2*1/2*2/3 = -2/3h
  L'0(x0) = -1/h*(1/-2 - 1 + 1 + /2) = 0
  L'1(X0) = 1/h*2/3*1/2*2 = 2/3h
  L'2(X0) = 1/2h*1/2*1/3*-1 = -1/12h
#}
function y = derivada5(f, x0, h)
  y = (f(x0-2*h) - f(x0+2*h))./(12*h) + 2*(f(x0+h) - f(x0-h))./(3*h);
endfunction

z = derivada5(@exp, 0, h);
loglog(h,abs(exp(0)-z));
legend("2 pontos", "3 pontos", "5 pontos", "location", "north");

display("\nValor de k arredondado para 5 pontos:");
k = round(abs(log10(abs( (exp(0) - z(2))/(exp(0) - z(1)) ))))

display("Valor de beta para 5 pontos:");
beta = mean(abs(exp(0) - z(1:3))/h(1:3).^k)
#{
  Olhando somente para o intervalo de valores em que o método usando 3 pontos não
 estoura ([10^-3, 10^-1]), conseguimos aproximar o valor de k e beta como sendo,
 respectivamente, 4 e 0.033373, como mostra o código acima.
#}

#{

Item 3:

Generalize sua implementação através de uma função 

function y = derivadaN(f,x0,h,N)

que use a fórmula da derivada do polinômio interpolador usando xl=x0+lh,...,x0,...,xu=x0+uh,
cobrindo N pontos em torno de x0 (use u=fix(N/2) e l=u-N+1). Aplique sua função aos dados
dos itens 1 e 2, ou seja, com N=2,3,5, e plote os erros em um novo gráfico, a fim de se
certificar de que fiquem bem parecidos (não se preocupe se obtiver alguns
"warning: axis: omitting non-positive data in log plot", isso se deve a estimativas exatas,
onde o erro é 0 e log=-∞).

#}
function dlj = findDLJs(left, right)
  dlj = [];
  for current = left:right
    if (current == 0)  
      sum = 0;
      for i = left:right
        if (i == 0)
          continue;
        endif
        sum += 1/i;
      endfor
      dlj = [dlj -1*sum];
    else
      prod = 1;
      for i = left:right
        if (i == current || i == 0)
          continue;
        endif
        prod *= i/(i - current);
      endfor
      dlj = [dlj (1/current)*prod];
    endif
  endfor
endfunction

function y = derivadaN(f, x0, h, N)
  right = fix(N/2);
  left = right-N+1;
  dlj = findDLJs(left, right);
  y = zeros(1, length(h));
  for j = left:right
    y += f(x0 + j.*h).*dlj(j+abs(left)+1);
  endfor
  y = y./h;
endfunction

figure(2);
hold on;
y = derivadaN(@exp,0,h, 2);
loglog(h,abs(exp(0)-y));
w = derivadaN(@exp,0,h, 3);
loglog(h,abs(exp(0)-w));
z = derivadaN(@exp, 0, h, 5);
loglog(h,abs(exp(0)-z));
xlabel("h");
ylabel("erro");
legend("2 pontos", "3 pontos", "5 pontos", "location", "north");
title("DerivadaN");
#{

Item 4:

Aplique sua função do item 3 com N=5,6,...,12, e observe os gráficos. Comente sobre
as ordens dos erros (a partir da inspeção visual e das primeiras estimativas com h=0.1 e h=0.01),
e sobre os pontos de "quebra" dos métodos. Qual N e qual h produziram o menor erro de todos?
Ofereça uma interpretação possível para os pontos de "quebra" acontecerem tão cedo nos últimos casos.

#}

currentFigure = 3;
titleString = "DerivadaN - ";
for n = 5:12
  if (n == 9)
     # Plota em uma outra figura os últimos 4 valores de n
     legend("5 pontos", "6 pontos", "7 pontos", "8 pontos", "location", "southwest");
     titleString = "DerivadaN - ";
     currentFigure++;
  endif
  figure(currentFigure);
  hold on;
  y = derivadaN(@exp, 0, h, n);
  loglog(h, abs(exp(0) - y));
  xlabel("h");
  ylabel("erro");
  titleString = [titleString num2str(n)];
  if (n != 12 && n != 8)
    titleString = [titleString ", "];
  endif
  title(titleString);
endfor
legend("9 pontos", "10 pontos", "11 pontos", "12 pontos", "location", "southwest");
#{
  Podemos notar que, de N = 5:7, os valores de k parecem assumir aproximadamente
 4, 5, 6, respectivamente. A partir de N = 8 em diante, conseguimos perceber que
 a qualidade do erro começa a piorar, assumindo valor de k aproximadamente 4 em N = 8,
 2 em N = 9 e piorando cada vez mais. A partir de N = 11, conseguimos notar que o erro,
 no geral, se torna praticamente cada vez maior conforme o h diminui, assumindo o seu 
 menor valor no primeiro h (10^-1). No geral, conseguimos ver que o menor erro foi obtido
 com N = 12 em h = 10^-1.
 
  É possível perceber que todos os gráficos possuem o seu ponto de "quebra" maior
 ou igual a 10^-3, piorando conforme o N aumenta, chegando até o estado em que o
 ponto de quebra já acontece no primeiro valor de h (10^-1). Isso deve ocorrer,
 provavelmente, pois conforme aumentamos o número de observações, mais erros de
 cancelamento ocorrem na hora de estimar o valor de f'(x0), fazendo com que o erro
 de arredodamento supere o erro da aproximação mais rápido.
#}

#{

Epílogo (para ler depois de entregar):

Se você quiser verificar as fórmulas do início do enunciado, vale a pena revisitar as
expressões das derivadas dos polinômios de Newton (nas anotações em pdf sobre as diferenças divididas),
pois as derivadas dos polinômios de Lagrange têm estrutura bastante parecida. Também pode ajudar
inspecionar os casos mais simples vistos no item 1, como faremos a seguir, para ganhar intuição.

Considerando apenas os pontos x0  e x1=x0+h, temos o polinômio interpolador de Lagrange

p2(x) = f(x0)·(x-x1)/(x0-x1) + f(x1)·(x-x0)/(x1-x0)
      = f(x0)·(x-x0-h)/(-h) + f(x0+h)·(x-x0)/h

de onde

p2'(x) = f(x0)/(-h) + f(x1)/h = (f(x0+h)-f(x0))/h

que é uma expressão constante (independe de x), o que é natural pois p2(x) é uma função afim.

Se usarmos 3 pontos x_1=x0-h, x0 e x1=x0+h, teremos

p3(x) = f(x_1)·(x-x0)(x-x1)/((x_1-x0)(x_1-x1))
        + f(x0)·(x-x_1)(x-x1)/((x0-x_1)(x0-x1))
        + f(x1)·(x-x_1)(x-x0)/((x1-x_1)(x1-x0))

de onde

p3'(x) = f(x_1)·[(x-x0)+(x-x1)]/((x_1-x0)(x_1-x1))
        + f(x0)·[(x-x_1)+(x-x1)]/((x0-x_1)(x0-x1))
        + f(x1)·[(x-x_1)+(x-x0)]/((x1-x_1)(x1-x0))

e calculando a derivada em x=x0 temos

p3'(x0) = f(x_1)·[-h]/(2h²)
          + f(x0)·[0]/(-h)
          + f(x1)·[h]/(2h²)
        = (f(x1)-f(x_1))/(2h).

Repetir a construção acima para o caso de 5 pontos do item 2 pode ajudar a vislumbrar
a generalização para o caso geral.

#}
