
# Identifique-se:
disp("Tarefa Prática 1")
disp("Nome Thiago Guerrero")
disp("NUSP: 11275297")

#{
Roteiro para a tarefa-prática 1:

1) escreva uma função myrand(M) com um gerador de números aleatórios positivos em
diversas ordens de grandeza: para isso use as funções rand() para gerar valores entre
[0,1] e randi([-M,M]) para gerar expoentes (em base 10). Você pode usar M=10 no restante desta tarefa.
#}
function rand = myrand(M)
  rand = rand()*10^randi([-M, M]);
endfunction
M=10

#{
2) escreva um par de funções xenc = enc(x,N) e x = dec(xenc,N) para codificar/decodificar 
um número fracionário positivo x em uma representação xenc=(m,e), usando uma variável inteira
m com N dígitos decimais significativos (alinhados à esquerda, ou seja, com o dígito associado
a 10^(N-1) sempre não-nulo) e um expoente e em base 10. Por exemplo, usando N=5, o valor x=123
seria codificado como m=12300 e e=-2 (pois x=12300*10^(-2)), enquanto o valor x=1.23456 seria
codificado como m=12346 e e=-4 (pois x~12346*10^(-4), sendo o último dígito o resultado de um
arredondamento). 
Teste sua função com alguns valores gerados pela função myrand(), copiando as saídas em comentários no código.
#}

function xenc = enc(x, N)
  e = (floor(log10(x)) + 1) - N;
  m = round(x*10^(N - 1 - floor(log10(x)))); #x*10^(-e)
  xenc = [m e];
endfunction

function x = dec(xenc, N)
  x = xenc(1)*10^xenc(2);
endfunction

N=5
#{
Número       | Saída
0.000059822  | [59822, -9]
97.730       | [97730, -3]
3216412.12057| [32164, 2]
5030.1       | [50301, -1]
4.8743       | [48743, -4]
#}

#{
3) escreva uma função encdec(x, N) para codificar e decodificar um valor x, devolvendo
o erro absoluto e o erro relativo produzidos no processo. Selecione três valores
aleatórios em ordens de grandeza diferentes (considere valores "pequenos" se e≤-6,
"médios" se -2≤e≤2 e "grandes" se e≥+6) e imprima o erro absoluto e o erro relativo
do processo de codificação+decodificação. Você percebe um padrão? Comente no código.
#}

function errors = encdec(x, N)
  xenc = enc(x, N);
  x_linha = dec(xenc, N);
  
  error_abs = abs(x - x_linha);
  errors = [error_abs, error_abs/abs(x)];
endfunction

# Pequenos
  values = [0.0000000091743 0.000000038328  0.051369];
  error = [];
  for i = 1:3
    error = [error; encdec(values(i), N)];
  endfor
  
# Médios
  values = [9499440.75886 7328559.98236 367.23];
  for i = 1:3
    error = [error; encdec(values(i), N)];
  endfor

# Grandes
  values = [18582821481.37942 64757915261.01232 63213700256.62754];
   for i = 1:3
    error = [error; encdec(values(i), N)];
  endfor
  display (error);
 
#{ 
  É possível observar que para valores muito pequenos quase não temos erro pois é possível
representar a mantissa no valor do N=5. O mesmo ocorre para alguns valores médios, como
367.23. Porém, é possível ver que para valores com uma parte decimal não nula muito grande,
o erro relativo começa a aumentar, mas ainda num valor bem baixo (cerca de 0.001%).
#}

#{
4) faça uma função testaencdec(N) que repita o experimento do item anterior 1000 vezes,
plotando os gráficos dos erros absolutos e relativos, e execute-a para N=3, 5 e 10.
Quais os valores máximos observados em cada caso? Você saberia explicá-los em função de M e N? 
Comente no código.
#}

function testaencdec(N)
  values = [];
  errors = [];
  for n=1:1000
    rand_number = myrand(10);
    values = [values rand_number];
    errors = [errors; encdec(rand_number, N)];
  endfor

  figure(1)
  loglog(values, errors(:, 1), ".r", values, errors(:, 2), ".g")
  xlabel("Valor Original");
  ylabel("Erros");
  legend("Erro Absoluto", "Erro Relativo", "location", "northwest");
  title(["Erros associados à utilização de ponto flutuante para N = " num2str(N)]);
endfunction
#testaencdec(3)
#{
  Valor Máximo: Cerca de 6e+06
#}

#testaencdec(5)
#{
  Valor Máximo: Cerca de 5e+05
#}

#testaencdec(10)
#{
  Valor Máximo: Cerca de 0.5
#}

#{
  É possível ver que o valor do N está diretamente relacionado com o tamanho do erro.
Essa análise é bem intuitiva pois, para valores de N muito menores do que M, temos um
arredondamento grande para caber no tamanho da mantissa. Já para valores de N e M praticamente
iguais, o erro é praticamente zero dado que a mantissa tem tamanho suficiente para armazenar os
números.
#}

#{
5) implemente uma função soma(x,y,N) que receba dois números xenc=(mx,ex) e yenc=(my,ey),
codificados com N dígitos significativos, e que produza um resultado zenc=(mz,ez),
também codificado. Para isso, decodifique as entradas, aplique a soma usual em octave e
codifique o resultado. Teste sua função com alguns pares (x,y) de valores aleatórios, e
selecione 3 exemplos onde (x,y) sejam respectivamente (pequeno,pequeno), (pequeno,grande)
e (grande,grande), registrando-os como comentários no código.
#}

function newValue = soma(x, y, N)
  decx = dec(x, N);
  decy = dec(y, N);
  newValue = decx + decy;
  newValue = enc(newValue, N);
endfunction

#{ 
(pequeno, pequeno) - (0.0000048490, 0.00075203)
ans =

   75688      -8
#}

#{ 
(pequeno, grande) - (445927558.86026, 0.000039290)
ans =

   44593       4
#}

#{
 (grande, grande) - (3116880881.34255, 14776265.95289)
 ans =

   31317       5
#}

#{
6) faça uma função testasoma(N) que produza 1000 somas de pares (x,y) de valores aleatórios,
e plote os erros absolutos e relativos considerando dois "ground truths" diferentes: (1) a
soma x+y realizada em octave, e (2) a expressão encdec(x)+encdec(y). Faça isso para N=3, 5 e 10.
Quais os valores máximos observados em cada caso? Qual a interpretação dos dois "ground truths"?
Comente no código.
#}
function testasoma(N)
  soma_real = [];
  soma_feiki = [];
  soma_encdec = [];

  for i=1:1000
    x = myrand(10);
    y = myrand(10);
    z = dec(soma(enc(x, N), enc(y, N), N), N);
    w = dec(enc(x, N), N) + dec(enc(y, N), N);
    soma_real = [soma_real (x+y)];
    soma_feiki = [soma_feiki z];
    soma_encdec = [soma_encdec w];
  endfor
  error_1 = abs(soma_real - soma_feiki);
  figure(1)
  loglog(soma_real, error_1, ".r", soma_real, error_1./abs(soma_real), ".g")
  xlabel("Valor Original da soma");
  ylabel("Erros");
  legend("Erro Absoluto", "Erro Relativo", "location", "northwest");
  title(["Erros associados à utilização da operação de soma em ponto flutuante para N = " num2str(N) " - Ground Truth 1"]);
  
  error_2 = abs(soma_encdec - soma_feiki);
  figure(2)
  loglog(soma_real, error_2, ".r", soma_real, error_2./abs(soma_encdec), ".g")
  xlabel("Valor Original da soma");
  ylabel("Erros");
  legend("Erro Absoluto", "Erro Relativo", "location", "northwest");
  title(["Erros associados à utilização da operação de soma em ponto flutuante para N = " num2str(N) " - Ground Truth 2"]);
endfunction

#testasoma(3)
#{
  Valor Máximo - Ground Truth 1: Cerca de 6.4e+06
  Valor Máximo - Ground Truth 2: Cerca de 5e+06
#}

#testasoma(5)
#{
  Valor Máximo - Ground Truth 1: Cerca de 5.6e+05
  Valor Máximo - Ground Truth 2: Cerca de 5e+05
#}

#testasoma(10)
#{
  Valor Máximo - Ground Truth 1: Cerca de 1.1
  Valor Máximo - Ground Truth 2: Cerca de 1
#}

#{
Qual a interpretação dos dois "ground truths"?
>> No primeiro ground truth temos, em média, um erro maior pois fazemos a comparação
considerando todos os erros de arredondamento já que "soma_real" representa o valor calculado
sem nenhum erro e o "soma_feiki" possui todos os erros de arredondamento.

Já no segundo ground truth, temos que a diferença entre "soma_encdec" e "soma_feiki"
não é, em média, tão grande, dado que ambas possuem erros de arrendodamento e a única diferença
é que "soma_feiki" possui um erro de arredondamento a mais.

É importante notar que em alguns casos a soma dos erros representado no segundo ground truth
acaba cobrindo o erro real observado no primeiro ground truth tendo como consequência alguns
valores maiores em comparação com o primeiro ground truth.
#}