#{
  Comparação de desempenho numérico das expressões
  f1(x) = ln(x - sqrt(x^2 - 1)) e f2(x) = -ln(x + sqrt(x^2 - 1))
#}
x = 10.^(1:20);
f1 = log(x - sqrt((x.**2) - 1))
f2 = -log(x + sqrt((x.**2) - 1))

loglog(x, f1, "-*b", x, f2, "-+r");
xlabel("x");
ylabel("Valores de f1 e f2");
legend("f1", "f2");
title("Comparação das expressões f1 e f2");
#{
    A fórmula mais segura numericamente é a expressão f2. Isso ocorre pois a expressão
  evita o erro de cancelamento que ocorre no termo x - sqrt(x^2 - 1) da expressão f1.
  Esse erro ocorre pois quando x^2 representa um valor muito maior do que 1, a expressão x^2 + 1,
  na representação numérica de números reais, acaba sendo exatamente igual a x^2, fazendo com 
  que sqrt(x^2 - 1) = sqrt(x^2) = |x|, resultando em ln(x - |x|) = ln(0) = -inf.
  
    Esse erro é nítido a partir de x = 10^8, onde todos os resultados usando a expressão f1 são
  iguais a -inf e somem do gráfico.
#}