2;
#{
  Qual fórmula para a variância produz o resultado com a maior acurácia
  f1 = (1/n)*sum(1, n){(x_i - x~)^2} ou f2 = (1/n)*sum(1, n){x_i^2} - x~^2
#}
format long e

# Funções Auxiliares
function xenc = enc(x,N)
  signal = sign(x);
  x = abs(x);
  if (x == 0)
    xenc = [0, 0];
  else
    e =  floor(log10(x))+1-N;
    m = round(x*10^(-e)); #x*10^(N-1-floor(log10(x)));
    xenc = [signal*m e];
  endif
endfunction

function x = dec(xenc,N)
  x = xenc(1)*10^xenc(2);
endfunction

N = 2;
x = 1:60;

#ground truth
x_var = var(x);

x_mean = dec(enc(mean(x), N), N);
x_mean_square = dec(enc(x_mean**2, N), N);

f1 = enc(0, N);
f2 = enc(0, N);
for i = 1:length(x)
  current_dec = dec(enc(x(i), N), N);
  current_dec_square = enc(current_dec**2, N);
  
  sub = dec(enc(current_dec - x_mean, N), N);
  sub_square = enc(sub**2, N);
  
  f1 = enc( dec(f1, N) + dec(sub_square, N) , N);
  f2 = enc ( dec(f2, N) + dec(current_dec_square, N) , N);
endfor

len = enc(length(x), N);
f1 = dec(enc( dec(f1, N)/dec(len, N) , N), N);

f2 = enc( dec(f2, N)/dec(len, N) , N);
f2 = dec(enc(dec(f2, N) - x_mean_square, N), N);

x_var
f1
f2
display(["Erro absoluto de f1:" num2str(abs(x_var - f1))]);
display(["Erro absoluto de f2:" num2str(abs(x_var - f2))]);