#{
Calcule f'(x0) evitando erros de cancelamento
para f(x) = sen(x) e x0 = 1.2 e compare com o resultado
da fórmula com erros de cancelamento.
#}
x0 = 1.2;
format long e
# f'(x0) = cos(x0) é o ground truth
valor_exato = cos(x0)

h = 10.^(0:-1:-20);
valor_aprox_ruim = (sin(x0+h) - sin(x0))./h

#{ 
  Usando a identidade trigonométrica temos:
  sen(x0 + h) - sen(x0) = 2*cos(x0 + h/2)*sen(h/2)
#}
valor_aprox_bom = (2*cos(x0 + (h./2)).*sin(h./2))./h

erro_abs_ruim = abs(valor_exato - valor_aprox_ruim);
erro_abs_bom = abs(valor_exato - valor_aprox_bom);

# f''(x0) = -sen(x0) => E = h*-sen(x0)/2
erro_teorico = abs(h.*-sin(x0)./2);

figure(1)
loglog(h, erro_abs_ruim, '-*', h, erro_teorico, 'r-.', h, erro_abs_bom, '-+m')
xlabel('h')
ylabel('erro absoluto e erro teórico')
legend("Erro Absoluto com cancelamento", "Erro Relativo", "Erro Absoluto sem cancelamento", "location", "southeast");
title("Aproximação de f'(x0) usando a aproximação de Taylor com e sem cancelamento")

#{
    É extremamente evidente que a manipulação algébrica usando a identidade trigonométrica
  resolve qualquer problema de arredondamento que a fórmula original possui no intervalo
  que definimos para o h. Isso se deve justamente pois o erro mais evidente era o de cancelamento e
  como a identidade resolveu esse erro, o único erro que temos acumulado é o das operações elementares
  sendo limitado por 10^-16 por operação.
  
    Usando o comando "format long e" para visualizar os números com uma maior precisão, é possível ver que
  o valor já converge para um valor aceitável de aproximação em torno de 10^-6 e que em torno de 10^15 o
  valor obtido pela aproximação é praticamente o mesmo valor obtido pelo ground truth, fazendo com que o
  erro absoluto seja 0 na sua representação.
#}