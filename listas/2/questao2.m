2;
#{
  Três implementações da expressão sum(1, 10^4){1/n}
#}
format long e

# Funções Auxiliares
function xenc = enc(x,N)
  e =  floor(log10(x))+1-N;
  m = round(x*10^(-e)); #x*10^(N-1-floor(log10(x)));
  xenc = [m e];
endfunction

function x = dec(xenc,N)
  x = xenc(1)*10^xenc(2);
endfunction

# Primeira Implementação
sum_1 = 0;
for i = 1:10^4
  sum_1 += 1/i;
endfor

# Segunda Implementação
N = 5;
sum_2 = enc(1, N);
for i = 2:10^4
  sum_2 = enc((1/i) + dec(sum_2, N), N);
endfor
sum_2 = dec(sum_2, N);

# Terceira Implementação
sum_3 = enc(1/10^4, N);
for i = (10^4)-1:-1:1
  sum_3 = enc((1/i) + dec(sum_3, N), N);
endfor
sum_3 = dec(sum_3, N);

[sum_1; sum_2; sum_3]

#{
    Como já esperado, é notório que a representação usando o sistema double teria
  uma precisão maior do que o sistema com 5 casas decimais. Porém, é interessante
  ver que a acurácia da terceira implementação chegou bem próximo do valor do sistema
  double se desconsiderarmos os dígitos menos significativos, diferente da segunda
  implementação que teve uma acurácia muito pior. Isso se deve pois, pegando como base
  o exemplo 2.3 do livro, é possível ver que, em geral, os sistemas de representação
  de números reais conseguem representar em maior quantidade números pequenos em detrimento
  de números muito grandes. Logo, quando fazemos a soma do jeito da segunda implementação,
  observamos um erro de arredondamento do tipo:
  
      fl(sum + 1/i) = sum 
      Onde fl representa a função enc para o sistema double e sum o valor acumulado até a iteração i.
      
    Justamente pois o sistema de representação é incapaz de representar com precisão
  o termo sum + 1/i e acaba arredondando para sum. A terceira implementação busca evitar isso,
  ela faz a soma começando com os termos pequenos e, como o sistema de representação consegue
  representar em maior quantidade valores pequenos, não temos um erro de arredondamento muito grande.
#}