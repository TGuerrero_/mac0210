#{
  Seja f em [a, b]. Dados n+1 pontos equidistantes x_i = a + ih, onde h = (b-a)/n
 e dado f'(a), construa um algoritmo para uma interpolação quadrática por trechos que
 seja de classe C^1. 
#}
2;
function coef = interpolation(xi, yi, dfa)
  n = length(xi)-1;
  h = xi(2)-xi(1);
  
  # s_i(x) = a_i + b_i*(x − x_i) + c_i*(x − x_i)^2
  coef = zeros(3, n); # coef(1, :) = a, coef(2, :) = b, coef(3, :) = c
  
  #s_0'(a) = f'(a) => b_0 = f'(a)
  coef(2, 1) = dfa;
  for i = 1:n
    # s_i(x_i) = f(x_i) => a_i = f(x_i)
    coef(1, i) = yi(i);

    #{
      Considerando que já temos b_i,
      s_i(x_{i+1}) = f(x_{i+1}) => b_i*h + c_i*h^2 = f(x_{i+1}) - f(x_i);
                                => c_i = (f(x_{i+1}) - f(x_i) - b_i*h)/h^2
    #}
    coef(3, i) = (yi(i+1) - yi(i) - coef(2, i)*h)/h^2;

    if (i < n)
      # s'_i(x-{i+1}) = s'_{i+1}(x_{i+1}) => b_i + c_i*h = b_{i+1};
      coef(2, i+1) = (coef(2, i) + coef(3, i)*h);
    endif
  endfor
endfunction

# Adiocional
function example()
  a = 1;
  b = 8;
  n = 3;
  h = round((b-a)/n);
  x = a:0.01:b;
  xi = a:h:b;
  yi = xi.^3;

  coef = interpolation(xi, yi, 3);
  pn = zeros(1, length(x));
  for i = 1:length(xi)-1
    if (i == length(xi)-1)
      range = lookup(x, xi(i)):length(x);
    else
      range = lookup(x, xi(i)):lookup(x, xi(i+1));
    endif
    pn(range) = coef(1, i) + coef(2, i)*(x(range) - xi(i)) + coef(3,i)*(x(range) - xi(i)).^2;
  endfor
  figure(1);
  plot(x, x.^3, "r", x, pn, "--g", xi, yi, "*");
endfunction
# Exemplo de uso (Descomentar se quiser usar)
#example()