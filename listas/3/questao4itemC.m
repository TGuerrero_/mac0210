%{
  f(x) = x^3 - a
  Calcule f(x) = 0 pelo método de Newton.
%}
2;
function [x0, n] = curt3(a, atol)
  n = 0;
  x0 = (a+2)/3;
  do
    if (dg(x0) < eps)
      display('Derivada nula');
      return;
    endif
    x0 = x0 - (g(x0, a)/dg(x0));
    n++;
  until abs(x0**3 - a) < atol
endfunction

function y = g(x, a)
  y = x**3 - a;
endfunction

function y = dg(x)
  y = 3*(x**2);
endfunction

%{
  |x_k+1 - x*| < e
  Porém, pela convergência quadrática temos:
  M*|x_k - x*|^2 < e
  M*(M*|x_k-1 - x*|^2)^2 < e => M^3*|x_k-1 - x*|^4 < e
  M*(M*(M*|x_k-2 - x*|^2)^2)^2 < e => M^7*|x_k-2 - x*|^8 < e
  M*(M*(M*(M*|x_k-3 - x*|^2)^2)^2)^2 < e => M^15*|x_k-3 - x*|^16 < e
  ...
  M^{2^{k+1}-1}*|x_0 - x*|^{2^{k+1}} < e
  log_M(M^{2^{k+1}-1}*|x_0 - x*|^{2^{k+1}}) < log_M(e)
  (2^{k+1}-1)*log_M(M) + (2^{k+1})*log_M(|x_0 - x*|) < log_M(e)
  2^{k+1} - 1 + (2^{k+1})*log_M(|(a+2)/3 - x*|) < log_M(e)
  2^{k+1} < (log_M(e) + 1)/(1 + log_M(|(a+2)/3 - x*|))
  (k+1)*log_2(2) < log_2((log_M(e) + 1)/(1 + log_M(|(a+2)/3 - x*|)))
  k < log_2(log_M(e) + 1) - log_2(1 + log_M(|(a+2)/3 - x*|)) - 1
  -k > -log_2(log_M(e) + 1) + log_2(1 + log_M(|(a+2)/3 - x*|)) + 1

  Assim, obtemos o número aproximado k de iterações.
%}