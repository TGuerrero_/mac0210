%{
  g(x) = x - x^3 - a
  Calcule g(x) = x pelo método do ponto fixo.
%}
2;
function [x0, n] = curt2(a, x0, atol)
 n = 0;
 do
   n++;
   x0 = g(x0, a);
 until abs(x0**3 - a) < atol
endfunction

function y = g(x, a)
  y = x - x**3 + a;
endfunction

function y = dg(x)
  y = 1 - 3*(x.**2);
endfunction

#{
  Como a função é contínua e a expressão de g'(x) não depende do valor de 'a', 
basta analisar apenas quais são os pontos em que podemos escolher um valor de rho
t.q |g'(x)| < rho < 1 e analisar em quais valores de 'a' o ponto fixo de g(x) está dentro
desse intervalo.

  Considerando os valores gerador pela função testValues abaixo, podemos ver que
a função tende a valores absolutos cada vez maiores na sua derivada (e se plotarmos 
o gráficos iremos ver que os extremos tendem ao infinito). Portanto, temos apenas um
intervalo de pontos x onde podemos garantir que a derivada pode ser limitada por um rho,
esse intervalo é aproximadamente ]0, 0.8]. Logo, analisando os valores de 'a' para esse
intervalo, temos que o método converge para 'a' aproximadamente perto do intervalo ]0, 0.5]. 
#}
function testValues()
  values = 0:0.1:3;
  dg(values)
endfunction