%{
  f(x) = x^3 - a
  Calcule f(x) = 0 pelo método da bisecção e calcule quantos passos são necessários
  para produzir uma solução com precisão 'atol'.
%}
2;
function [p, n] = curt1(a, atol)
  inicio = a;
  fim = 1;
  if (a > 1)
    inicio = 1;
    fim = a;  
  endif
  
  fa = f(inicio, a);
  fb = f(fim, a);
  # Bisecção
  if (inicio >= fim) || (fa*fb >= 0) || (atol <= 0)
    disp(' something wrong with the input: quitting');
    p = NaN; n=NaN;
    return;
  endif

  n = ceil ( log2 (abs(1 - a)) - log2 (2*atol));
  for k=1:n
    p = (inicio+fim)/2;
    fp = f(p, a);
    if (abs(fp) < eps) 
      n = k; 
      return; 
    endif
    if (fa * fp < 0)
      fim = p;
    else
      inicio = p;
      fa = fp;
    endif
  endfor
  p = (inicio+fim)/2;
endfunction

function y = f(x, a)
  y = x**3 - a;
endfunction

%{
  Considere o intervalo [inicio, fim]. Temos que, para a iteração 'k' e considerando
x* como solução:
  |x_k - x*| <= (fim - inicio)/2 * (1/2)^k

Como queremos um erro limitado por 'atol', temos que:
  (fim - inicio)/2 * (1/2)^k < atol  <=>  log_2(|fim - inicio|/(2*atol)) < k
 
Considerando os intervalos [a, 1] ou [1, a] podemos dizer, sem perda de generalidade
que:
  k > log_2(|1 - a|) - log_2(2*atol)
%}

