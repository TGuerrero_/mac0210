#{
  Conisdere a expressão
  f(x) = (x-2)⁹ = x⁹ - 18x⁸ + 144x⁷ - 672x⁶ + 2016x⁵ - 4032x⁴ + 5376x³ - 4608x² + 2304x - 512
  Calcule usando a regra de Horner e usando a função de potência do Octave
#}
x = 1.921:0.00098758:2.08;

# Horner
coeficient = [-512, 2304, -4608, 5376, -4032, 2016, -672, 144, -18, 1];
n = length(coeficient);
f1 = coeficient(n);
for i = length(coeficient)-1:-1:1
  f1 = f1.*x + coeficient(i);
endfor

# Diretamente
f2 = (x - 2).**9;

figure(1);
plot(x, f1, "r");
xlabel('x');
ylabel('f(x)');
title('Regra de Horner');
figure(2);
plot(x, f2, "g");
xlabel('x');
ylabel('f(x)');
title('Biblioteca Matemática');

#{
  É possível ver que o comportamento da segunda implementação possui uma consistência
muito melhor do que a implementação usando a regra de Horner. Isso deve acontecer pois,
na primeira implementação, temos o acumulo dos erros das operações elementares sendo
multiplicados a cada iteração fazendo com que o gráfico fique com algumas variações
em relação ao seu valor original. Já a segunda implementação, considerando que a 
biblioteca matemática foi feita da forma mais otimizada e acurada possível, temos uma
implementação que irá utilizar um mecânismo com o menor acumulo de erros possível, gerando
um gráfico mais estável.
#}

