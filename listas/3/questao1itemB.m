#{
  Conisdere a expressão
  f(x) = (x-2)⁹ = x⁹ - 18x⁸ + 144x⁷ - 672x⁶ + 2016x⁵ - 4032x⁴ + 5376x³ - 4608x² + 2304x - 512
  Ache as raízes com o método da Bisecção usando as duas fórmulas.
#}
x = 1.921:0.00098758:2.08;
error = 1e-6;

# Horner
function y = horner(x)
  coeficient = [-512, 2304, -4608, 5376, -4032, 2016, -672, 144, -18, 1];
  n = length(coeficient);
  y = coeficient(n);
  for i = length(coeficient)-1:-1:1
    y = y*x + coeficient(i);
  endfor
endfunction

[p1, n1] = bisect(@horner, 1.921, 2.08, horner(1.921), horner(2.08), error)

# Diretamente
[p2, n2] = bisect(inline('(x - 2).**9'), 1.921, 2.08, (1.921 - 2)**9, (2.08 - 2)**9, error)

#{
  Conseguimos ver que o método da Bisecção usando o segundo método é muito mais 
preciso e eficaz do que o usando a regra de Horner. Como vimos na resposta do item a,
temos que a regra de Horner possui muitas variações do valor original, dificultando o
método da bisecção para chegar na sua condição de parada, fazendo com que o método execute
mais vezes e provavelmente não retorne o valor mais próximo da solução real. Já o segundo
método conseguiu produzir o resultado de forma rápida justamente por conta da sua otimização
de implementação, precisando de somente 1 passo para encontrar a solução aproximada.
#}