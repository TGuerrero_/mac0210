#{
  Obtendo a aproximação de f'''(0) de e^x usando a aproximação
 encontrada no item a
#}

h = 10.^(-1:-1:-9);
y = e^0;
f =  (3*e.^(-h) - 3*e.^h + (3/2)*e.^(2*h) - (3/2)*e.^(-2*h))./(3*h.^3)
err = abs(y - f);

subplot (1, 2, 1)
loglog(h, ones(length(h))*y, 'r', h, f, '--g');
title("Função original x Aproximação");
xlabel("h");
ylabel("f(x)");
legend("Função", "Aproximação");

subplot (1, 2, 2)
loglog(h, err);
title("Erro da aproximação");
xlabel("h");
ylabel("Erro");
legend("Erro");

#{
  Pelo gráfico do erro, conseguimos ver que sempre que diminuimos o h 10 vezes,
 o erro produzido é reduzido 100 vezes (Nos pontos onde o algoritmo não quebra).
 
  Além disso, podemos ver que o melhor valor de h é 10^-3.
#}