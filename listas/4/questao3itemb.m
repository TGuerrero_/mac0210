#{
  Temos que, usando a notação dos coeficientes de Newton:
  gamma_0 = e^{c_0 - c_1*x_0 + c_2*x_0*x_1}
  gamma_1 = c_1 - c_2*(x_1 + x_0)
  gamma_2 = c_2
#}
2;
function g = gamma1(x, c0, c1, c2)
  g = e^(c0 - c1*x(1) + c2*x(1)*x(2));
endfunction

function g = gamma2(x, c1, c2)
  g = c1 - c2*(x(2) + x(1));
endfunction

interval = 0:0.1:6;
x = [0, 1, 3];
y = [1, 0.9, 0.5];
z = log(y);

# Pela base de Newton
c0 = z(1);
c1 = (z(2) - z(1))/(x(2) - x(1));
c2 = (z(3) - z(1) - c1*(x(3) - x(1)))/((x(3) - x(1))*(x(3) - x(2)));

g0 = gamma1(x, c0, c1, c2);
g1 = gamma2(x, c1, c2);
g2 = c2;
display(["Gamma 0: " num2str(g0) ; "Gamma 1: " num2str(g1) ; "Gamma 2: " num2str(g2)]);

u = g0*e.^(g1*interval + g2*interval.^2);
plot(interval, u, "r", x(1), y(1), "*g", x(2), y(2), "*g", x(3), y(3), "*g");
xlabel("x");
ylabel("u(x)");
title("Polinômio interpolador de u(x)");

#{
    Podemos ver que a curva está muito mais próxima de uma função linear do que de
  uma função quadrática nesse intervalo. Além disso, os valores de x crescem para +inf
  e -inf com valores de y sempre positivos proxímos de zero e, analisando a expressão, 
  temos que de fato nunca teremos uma raiz, diferente de uma função quadrática (com Delta >= 0)
  que possui no mínimo uma raiz e cresce nos valores de y para +inf ou -inf.
#}