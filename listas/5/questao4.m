#{
  item b
  
  Interpolação da função f(x) = e^x no intervalo [0, 1] para as observações
  xi = [0, 1/2, 1]
#}
2;
function c = diffDiv(xi, yi)
  n = length(xi);
  if (n == 1)
     c = yi(1);
  else
     c = (diffDiv(xi(2:n), yi(2:n)) - diffDiv(xi(1:n-1), yi(1: n-1)))/(xi(n) - xi(1));
  endif
endfunction

function p = newton(x, xi, yi)
  base = 1;
  c = zeros(1, length(xi));
  for i = 1:length(xi)
    c(i) = diffDiv(xi(1:i), yi(1:i));
  endfor
  p = c(length(xi))*ones(1, length(x));
  for i = length(xi)-1:-1:1
    p = c(i) + (x - xi(i)).*p;
  endfor
endfunction

x = 0:0.01:1;
xi = [0, 1/2, 1];
yi = [1, sqrt(e), e];

f = e.^x;
p = newton(x, xi, yi);
figure(1);
plot (x, f, "r", x, p, "--g", xi, yi, "*b");
legend("f(x)", "p(x)", "xi");

#{
  item c
  
  Análise do erro da interpolação do item b
#}
emax = e/6 * 2716/56451 # item a
erro = abs(f-p);
figure(2);
semilogy(x, erro, "m", x, emax*ones(1, length(x)), "--r");
title("Erro da interpolação do item b");
xlabel("x");
ylabel("Erro");
legend("Erro", "Erro Máximo Teórico");