#{
  Item a
  
  Interpolação por trechos [x_i, x_{i+1}) em n+1 observações
#}
2;

function y = lagrange(x,xi,yi)
  ## calcula o polinômio interpolador associado às observações
  ## (xi,yi) nos pontos x, usando a versão baricêntrica da
  ## interpolação de Lagrange.
  ## 1) calcula os pesos baricêntricos p(i) = 1/Π(xi-xj) (j≠i)
  p = ones(1,length(xi));
  for i = 1:length(p)
    for j = [1:i-1 i+1:length(p)]
      p(i) /= (xi(i)-xi(j));
    endfor
  endfor
  ## 2) calcula para cada x(j) o valor y(j) do polinômio interpolador
  y = zeros(1,length(x));
  for j = 1:length(x)
    ## calcula numerador = Σ yi*p(i)/(x-xi) e denominador = Σ p(i)/(x-xi)
    num = den = 0;
    flag = 0; ## usado para identificar se x=xi para algum i
    for i = 1:length(xi)
      if abs(x(j)-xi(i))<eps ## se x(j)=xi, então y(j) deve ser yi
        y(j) = yi(i);
        flag = 1; ## força a interrupção do laço
        break;
      endif
      temp = p(i)/(x(j)-xi(i)); ## termo comum ao numerador e denominador
      num += yi(i)*temp;
      den += temp;
    endfor
    if flag == 0 ## se o laço anterior terminou normalmente
      y(j) = num/den; ## aplica a fórmula baricêntrica
    endif
  endfor
endfunction

function pns = interpolation(x, xi, yi)
  n = length(xi);
  pns = zeros(1, length(x));
  for i = 1:n-1
    range = lookup(x, xi(i)):lookup(x, xi(i+1))-1;
    if (i == 1)
      lxi = xi(i:i+3); 
      lyi = yi(i:i+3);
    elseif(i == n-1)
      lxi = xi(i-2:i+1);
      lyi = yi(i-2:i+1);
    else
      lxi = xi(i-1:i+2);
      lyi = yi(i-1:i+2);
    endif
    pns(range) = lagrange(x(range), lxi, lyi);
  endfor
endfunction

#{
  Item b
  
  Aplicando o método do item a para n+1 observações
#}
step = 1/1000;
x = 0:step:1;
y = 10*x.^3 - floor(10*x);

xi = 0:0.02:1;
yi = 10*xi.^3 - floor(10*xi);

pns = interpolation(x, xi, yi);

figure(1)
plot(x, y, "r", x, pns, "--g", xi, yi, "*b");
legend("f(x)", "p(x)", "xi", "location", "north");
title("Interpolação Normal");

#{
    Podemos notar que a função f(x) possui diversos pontos de inflexão com um comportamento
  não suave, ou seja, conseguimos ver uma mudança brusca de direção. Entretanto, observando
  o polinômio interpolador, conseguimos notar que ele possui um comportamento muito mais suave
  do que a função original nesses pontos. É possível notar também que nenhuma observação xi está
  posicionada em um dos pontos críticos de máximo local da função, somente nos pontos de mínimo
  local, o que causa um maior distanciamento da função original, já que nesses pontos temos essa
  mudança de comportamento brusca na função original.
#}


#{
  Item c
  
  Interpolação por trechos [x_i, x_{i+1}) em n+1 observações usando uma nova heurística.
  
  Heurística:
  Para produzir o comportamento brusco da função original, essa nova heurística
  muda o seu comportamento em alguns pontos:
  
  - Se y(i+1) for menor do que y(i), olhamos somente para as observações anteriores a x(i).
  Pois sempre que temos um valor menor, vemos que houve uma mudança brusca de valores da função 
  entre x(i) e x(i+1), assim, olhar para as observações à frente de x(i) irá fazer com que o 
  polinômio precise ser mais suave para interpolar todos os pontos.
  
  - Se a distância absoluta entre y(i-1) e y(i) for menor que 0.6, olhamos somente para as
  observações posteriores a x(i). Essa mudança faz com que os pontos de mínimo local evitem ao
  máximo olhar para os pontos anteriores à mudança brusca, produzindo melhores aproximações.
  
  - Se a distância absoluta entre y(i+1) e y(i+2) for menor que 0.6, olhamos somente para as
  observações anteriores a x(i). A explicação é parecida com o caso anterior, mas nesse caso
  evitamos ao máximo que um ponto anterior a uma mudança brusca utlize observações de antes e
  depois da mudança.
#}


function pns = interpolation_heuristic(x, xi, yi)
  n = length(xi);
  pns = zeros(1, length(x));
  for i = 1:n-1
    range = lookup(x, xi(i)):lookup(x, xi(i+1))-1;
    if (i == 1)
      lxi = xi(i:i+3); 
      lyi = yi(i:i+3);
    elseif(i == n-1)
      if (yi(i+1) - yi(i) < 0)
        lxi = xi(i-3:i);
        lyi = yi(i-3:i);
      else
        lxi = xi(i-2:i+1);
        lyi = yi(i-2:i+1);
      endif
    else
      if (i > 3 && yi(i+1) - yi(i) < 0)
        #1
        lxi = xi(i-3:i);
        lyi = yi(i-3:i);
      elseif (i < n-2 && abs(yi(i-1) - yi(i)) > 0.6)
        #2
        lxi = xi(i:i+3);
        lyi = yi(i:i+3);
      elseif (i > 3 && abs(yi(i+1) - yi(i+2)) > 0.6)
        #3
        lxi = xi(i-3:i);
        lyi = yi(i-3:i);
      else
        lxi = xi(i-1:i+2);
        lyi = yi(i-1:i+2);
      endif

    endif
    pns(range) = lagrange(x(range), lxi, lyi);
  endfor
endfunction

step = 1/1000;
x = 0:step:1;
y = 10*x.^3 - floor(10*x);

xi = 0:0.02:1;
yi = 10*xi.^3 - floor(10*xi);

pns = interpolation_heuristic(x, xi, yi);
figure(2)
plot(x, y, "r", x, pns, "--g", xi, yi, "*b");
legend("f(x)", "p(x)", "xi", "location", "north");
title("Interpolação com a nova heurística");