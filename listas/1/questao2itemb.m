#{
Calcule o erro absoluto da aproximação de Taylor para f(x) = sen(x) e x0 = 1.2

aproximação de Taylor:
f'(x0) ~ (f(x0+h)) - f(x0-h)/2h
f'(x0) ~ (sen(x0+h) - sen(x0-h))/2h
#}
x0 = 1.2;

# f'(x0) = cos(x0) é o ground truth
valor_exato = cos(x0)

h = 10.^(-1:-1:-7);
valor_aprox = (sin(x0+h) - sin(x0-h))./(2.*h)

erro_abs = abs(valor_exato - valor_aprox);

[h; erro_abs]

#{
    É possível ver que o erro absoluto usando a aproximação de Taylor do exercício
  produz erros menores em comparação com a tabela original, sendo essa diferença
  exponencial, dado que o erro desse exercício é da ordem de O(h^2) e o da tabela original
  é da ordem de O(h). Entretanto, é possível ver que a partir de 1e-6 o erro começa
  a aumentar de tamanho em vez de diminuir, provavelmente por algum erro de arredondamento
  tomando conta da aproximação. Porém, mesmo com esse aumento, o valor do erro ainda
  é menor do que o exibido na tabela original.

#}