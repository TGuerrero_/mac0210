#{
Calcule f'(x0) 
para f(x) = e^(-2x) e x0 = 0.5

f'(x0) ~ (f(x0 + h) - f(x0))/h - h[f''(x0)/2] 
f'(x0) ~ (e^(-2(x0 + h)) - e^(-2x0))/h - E, onde E  = h[f''(x0)/2] 
#}
x0 = 0.5;

# f'(x0) = -2*e^(-2x0) é o ground truth
valor_exato = -2*e^(-2*x0)

h = 10.^(-1:-1:-20);

valor_aprox = (e.^(-2*(x0 + h)) - e.^(-2*x0))./h

erro_abs = abs(valor_exato - valor_aprox);

# f''(x0) = 4*e^(-2x0) => E = 2h*[e^(-2x0)]
erro_teorico = 2*h*e^(-2*x0);

loglog(h,erro_abs,'-*',h,erro_teorico,'r-.')
xlabel('h')
ylabel('erro absoluto e erro teórico')
title("Aproximação de f'(x0) usando Taylor")

#{
    A primeira diferença que é notória é que o gráfico desse exercício não possui
  um erro de discretização muito grande para valores pequenos de h em comparação com
  o exemplo 1.3, ou seja, até o momento em que o erro de arredondamento toma conta
  do gráfico, o erro absoluto e o erro teórico seguem praticamente a mesma linha sem
  nenhuma variação, diferente do exemplo 1.3, que no intervalo [10-8, 10-7] possui um
  erro alto de discretização.
  
    Em questão de similaridade, é possível ver que ambos os exemplos tem o seu comportamento
  completamente dominado pelo erro de arredondamento a partir de 10-8, sendo o gráfico desse
  exercício demora cerca de 10^(-1) de diminuição a mais que o h do exemplo para atingir o valor
  mais próximo de 0.
#}