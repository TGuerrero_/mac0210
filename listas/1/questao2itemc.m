#{
Calcule f'(x0)
para f(x) = sen(x) e x0 = 1.2
usando a aproximação de Taylor
f'(x0) ~ (f(x0+h)) - f(x0-h)/2h - (h^2)*f'''(x0)/6
f'(x0) ~ (sen(x0+h) - sen(x0-h))/2h - E, onde E = (h^2)*f'''(x0)/6
#}
x0 = 1.2;

# f'(x0) = cos(x0) é o ground truth
valor_exato = cos(x0)

h = 10.^(-1:-1:-20);
valor_aprox = (sin(x0+h) - sin(x0-h))./(2.*h)

erro_abs = abs(valor_exato - valor_aprox);

# f'''(x0) = -cos(x0) => E = (h^2)*-cos(x0)/6
erro_teorico = abs((h.^2).*(-cos(x0))/6);

loglog(h, erro_abs, '-*', h, erro_teorico, 'r-.')
xlabel('h')
ylabel('erro absoluto e erro teórico')
title("Aproximação de f'(x0) usando a aproximação de Taylor do exercício")

#{
    A primeira grande similaridade é que ambos os gráficos são dominados pelo erro
  de arredondamento a partir de um h pequeno. Na aproximação do exercício é possível
  ver que esse h é próximo de 1e-6 (igual notamos no item b) enquanto na aproximação
  do exemplo 1.3 esse h é próximo de 1e-8.
    
    É importante notar que embora o erro de discretização do exercício seja menor
  do que o erro notado no exemplo 1.3, o erro de arredondamento acaba sendo muito
  mais evidente e presente no erro acumulado do que no exemplo. Isso se deve pela
  utilização de mais float operations em relação ao modelo do exemplo.
#}